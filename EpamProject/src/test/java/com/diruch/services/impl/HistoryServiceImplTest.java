package com.diruch.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.diruch.converters.HistoryConverter;
import com.diruch.dto.HistoryDto;
import com.diruch.models.History;
import com.diruch.repository.HistoryRepository;
import com.diruch.services.HistoryService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class HistoryServiceImplTest {

    @Mock
    private HistoryRepository historyRepository;

    @Mock
    private HistoryConverter historyConverter;

    private static final int ID = 1;

    @InjectMocks
    private HistoryService historyService = new HistoryServiceImpl();

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getHistoryByTicketId() throws Exception {
        History history = new History();
        HistoryDto historyDto = new HistoryDto();
        List<History> histories = new ArrayList<>();
        List<HistoryDto> historyDtos = new ArrayList<>();
        histories.add(history);
        historyDtos.add(historyDto);
        when(historyRepository.getHistoryByTicketId(ID)).thenReturn(histories);
        when(historyConverter.convertToDto(history)).thenReturn(historyDto);
        assertEquals(historyDtos, historyService.getHistoryByTicketId(ID));
    }

}