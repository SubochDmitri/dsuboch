package com.diruch.services.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import com.diruch.converters.CommentConverter;
import com.diruch.dto.CommentDto;
import com.diruch.models.Comment;
import com.diruch.repository.CommentRepository;
import com.diruch.services.CommentService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CommentServiceImplTest {

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private CommentConverter commentConverter;

    @InjectMocks
    private CommentService commentService = new CommentServiceImpl();

    private static final int ID = 1;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getCommentsByTicketId() throws Exception {
        Comment comment = new Comment();
        CommentDto commentDto = new CommentDto();
        List<Comment> comments = new ArrayList<>();
        List<CommentDto> commentDtos = new ArrayList<>();
        commentDtos.add(commentDto);
        comments.add(comment);
        when(commentRepository.getCommentsByTicketId(ID)).thenReturn(comments);
        when(commentConverter.convertToDto(comment)).thenReturn(commentDto);
        assertEquals(commentDtos, commentService.getCommentsByTicketId(ID));
    }

}