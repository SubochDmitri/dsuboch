package com.diruch.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.diruch.models.Attachment;
import com.diruch.repository.AttachmentRepository;
import com.diruch.services.AttachmentService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class AttachmentServiceImplTest {

    @Mock
    private AttachmentRepository attachmentRepository;

    @InjectMocks
    private AttachmentService attachmentService = new AttachmentServiceImpl();

    private static final int ID = 1;

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAttachmentsByTicketId() throws Exception {
        List<Attachment> list = new ArrayList<>();
        when(attachmentRepository.getAttachmetsByTicketId(ID)).thenReturn(list);
        assertEquals(list, attachmentService.getAttachmentsByTicketId(ID));
    }

    @Test
    public void getAttachmentById() throws Exception {
        Attachment attachment = new Attachment();
        when(attachmentRepository.getAttachmentById(ID)).thenReturn(attachment);
        assertEquals(attachment, attachmentService.getAttachmentById(ID));
    }


}