package com.diruch.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.diruch.exceptions.AccessErrorException;
import com.diruch.exceptions.InvalidJsonException;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.repository.TicketRepository;
import com.diruch.services.TicketService;
import com.diruch.validator.TicketValidator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.multipart.MultipartFile;

public class TicketServiceImplTest {

    @Mock
    private TicketRepository ticketRepository;

    @Mock
    private TicketValidator validator;

    private static final int ID = 1;

    @InjectMocks
    private TicketService ticketService = new TicketServiceImpl();

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = AccessErrorException.class)
    public void submit() throws Exception {
        Ticket ticket = new Ticket();
        User user = new User();
        when(validator.isValidForSubmit(ticket, user)).thenReturn(false);
        ticketService.submit(ticket, user);
    }

    @Test
    public void getTicketById() throws Exception {
        Ticket ticket = new Ticket();
        when(ticketRepository.getTicketById(ID)).thenReturn(ticket);
        assertEquals(ticket, ticketService.getTicketById(ID));
    }

    @Test(expected = InvalidJsonException.class)
    public void createTicket() throws Exception {
        MultipartFile[] files = new MultipartFile[1];
        ticketService.createTicket("asd", "", files);
    }

}