package com.diruch.services.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import com.diruch.converters.TicketConverter;
import com.diruch.converters.UserConverter;
import com.diruch.dto.TicketDto;
import com.diruch.dto.UserDto;
import com.diruch.enums.Role;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.repository.UserRepository;
import com.diruch.services.UserService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private TicketConverter ticketConverter;

    @Mock
    private UserConverter userConverter;

    private static final String USER_NAME= "user";

    @InjectMocks
    private UserService userService = new UserServiceImpl();

    @Before
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void loadUserByUsername() throws Exception {
        User user = new User();
        user.setEmail(USER_NAME);
        when(userRepository.loadUserByUsername(USER_NAME)).thenReturn(user);
        assertEquals(USER_NAME,userService.loadUserByUsername(USER_NAME).getEmail());
    }

    @Test
    public void getManagerTicketsDto() throws Exception {
        User user = new User();
        List<TicketDto> ticketDtos = new ArrayList<>();
        TicketDto ticketDto = TicketDto.builder()
            .owner(user)
            .build();
        ticketDtos.add(ticketDto);
        List<Ticket> tickets = new ArrayList<>();
        Ticket ticket = new Ticket();
        ticket.setOwner(user);
        tickets.add(ticket);
        when(userRepository.getManagerTickets(user)).thenReturn(tickets);
        when(ticketConverter.convertListInDto(tickets)).thenReturn(ticketDtos);
        assertEquals(ticketDtos,userService.getManagerTicketsDto(user));
    }

    @Test
    public void getCurrentUser() throws Exception{
        UserDto userDto = new UserDto();
        User user = new User();
        user.setRole(Role.EMPLOYEE);
        user.setEmail(USER_NAME);
        List<TicketDto> myTickets = new ArrayList<>();
        List<TicketDto> allTickets = new ArrayList<>();
        when(userService.loadUserByUsername(USER_NAME)).thenReturn(user);
        when(userService.getOwnerTicketsDto(user)).thenReturn(allTickets);
        when(userConverter.convertToDto(user,allTickets,myTickets)).thenReturn(userDto);
        assertEquals(userDto, userService.getCurrentUser(USER_NAME));
    }


    @Test
    public void getAllEngineers() throws Exception {
        List<User> users = new ArrayList<>();
        when(userRepository.getAllEngineers()).thenReturn(users);
        assertEquals(users, userService.getAllEngineers());
    }

}