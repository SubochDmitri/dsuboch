INSERT INTO user (id, first_name, last_name, position, phone, role_id, email, password, adress) VALUES (
  NULL, 'Ivan', 'Ivanovich', 'Big BOSS', '123-45-67', 0, 'user1_mogilev@yopmail.com', 'P@ssword1', 'Mogilev, Belarus'
);
INSERT INTO user (id, first_name, last_name, position, phone, role_id, email, password, adress) VALUES (
  NULL, 'Petr', 'Petrovich', 'Typical user', '987-65-43', 0, 'user2_mogilev@yopmail.com', 'P@ssword1', 'Mogilev, Belarus'
);
INSERT INTO user (id, first_name, last_name, position, phone, role_id, email, password, adress) VALUES (
  NULL, 'Dmitri', 'Neizvestni', 'Manager', '845-64-37', 1, 'manager1_mogilev@yopmail.com', 'P@ssword1',
  'Mogilev, Belarus'
);
INSERT INTO user (id, first_name, last_name, position, phone, role_id, email, password, adress) VALUES (
  NULL, 'Nikolai', 'Grozni', 'Manager', '613-12-63', 1, 'manager2_mogilev@yopmail.com', 'P@ssword1',
  'Mogilev, Belarus'
);
INSERT INTO user (id, first_name, last_name, position, phone, role_id, email, password, adress) VALUES (
  NULL, 'Iosif', 'Stalin', 'Engineer', '523-12-54', 2, 'engineer1_mogilev@yopmail.com', 'P@ssword1', 'Mogilev, Belarus'
);
INSERT INTO user (id, first_name, last_name, position, phone, role_id, email, password, adress) VALUES (
  NULL, 'Vladimir', 'Lenin', 'Engineer', '123-54-12', 2, 'engineer2_mogilev@yopmail.com', 'P@ssword1',
  'Mogilev, Belarus'
);