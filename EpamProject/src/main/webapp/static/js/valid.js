$(document).ready(function () {
    $("input#search").bind('keypress', validate);
    $("input#ticketName").bind('keypress', validateName);
    $("textarea#ticketDescription").bind('keypress', validate);
    $("textarea#comment").bind('keypress', validate);
});

function validate(event) {
    var char = String.fromCharCode(event.which);
    if (!/[a-zA-Z0-9~.\x22(),:;<>@[\]!#$%&\x27*+-=\/?^_`{|}]/.test(char)) {
        return false;
    }
}

function validateName(event) {
    var char = String.fromCharCode(event.which);
    if (!/[a-z0-9~.\x22(),:;<>@[\]!#$%&\x27*+-=\/?^_`{|}]/.test(char)) {
        return false;
    }
}

