var a = [];

function exist(id) {
    for (var i = 0;i< a.length; i++){
        if (a[i]==id) return true;
    }
}

$(document).ready(function () {
    $('body').on('click', '#delete', function () {
        var num = $(this).attr("name");
        var s = 'p#' + num;
        $(s).attr('hidden', 'hidden');
        a.push(num);
        console.log(a);
    });
    $('body').on('change', '#file', function () {
        var file_num = document.getElementById("file").files.length;
        for (var i = 0; i < file_num; i++) {
            $('.files').html($('.files').html() + '<p id="' + i + '">' + $(
                "#file")[0].files[i].name
                + ' <img src="https://pp.userapi.com/c840331/v840331575/5c04d/iemFiWK8k8E.jpg" id="delete" name="' + i
                + '"></p>');

        }
    });
});

function updateSize() {
    var nBytes = 0,
        oFiles = document.getElementById("file").files,
        nFiles = oFiles.length;
    var bool = false;
    for (var nFileId = 0; nFileId < nFiles; nFileId++) {
        nBytes = oFiles[nFileId].size;
        if (nBytes > 5 * 1024 * 1024) {
            bool = true;
        }
    }

    if (bool) {
        document.getElementById("file").value = "";
        document.getElementById(
            "error").innerHTML = "The size of attached file should not be greater than 5 Mb. Please select another file.";
    }
}

function getValue(array, search) {
    for (var key in array) {
        if (key == search) {
            return array[key];
        }
    }
}

function convertToEng(s) {
    var array = {
        '1040': 'A',
        '1041': '6',
        '1042': 'B',
        '1043': 'r',
        '1044': 'g',
        '1045': 'E',
        '1046': 'j',
        '1047': '3',
        '1048': 'u',
        '1049': 'u',
        '1050': 'K',
        '1051': 'IL',
        '1052': 'M',
        '1053': 'H',
        '1054': 'O',
        '1055': 'n',
        '1056': 'P',
        '1057': 'C',
        '1058': 'T',
        '1059': 'Y',
        '1060': 'F',
        '1061': 'X',
        '1062': 'C',
        '1063': 'CH',
        '1064': 'SH',
        '1065': 'SH',
        '1066': 'b',
        '1067': 'bl',
        '1068': 'b',
        '1069': 'E',
        '1070': 'U',
        '1071': 'YA',
        '1072': 'a',
        '1073': '6',
        '1074': 'B',
        '1075': 'r',
        '1076': 'g',
        '1077': 'e',
        '1078': 'j',
        '1079': '3',
        '1080': 'u',
        '1081': 'u',
        '1082': 'k',
        '1083': 'IL',
        '1084': 'm',
        '1085': 'H',
        '1086': 'o',
        '1087': 'n',
        '1088': 'p',
        '1089': 'c',
        '1090': 'T',
        '1091': 'y',
        '1092': 'f',
        '1093': 'x',
        '1094': 'c',
        '1095': 'ch',
        '1096': 'sh',
        '1097': 'sh',
        '1098': 'b',
        '1099': 'bl',
        '1100': 'b',
        '1101': 'e',
        '1102': 'u',
        '1103': 'ya'
    };
    var s2 = "";
    for (var i = 0; i < s.length; i++) {
        var code = s.charCodeAt(i);
        var value = getValue(array, code);
        if (value) {
            s2 += value;
        }
        else {
            s2 += s.charAt(i);
        }
    }
    return s2;
}