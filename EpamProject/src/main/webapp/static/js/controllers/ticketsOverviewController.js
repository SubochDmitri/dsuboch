app.controller('TicketOverviewCtrl',
    ['$http', '$scope', '$window', 'TicketsService', 'FeedbackService',
        function ($http, $scope, $window, TicketsService, FeedbackService) {

            var path = $window.location.pathname.split('/');
            var ticketId = path[path.length - 1];
            console.log(ticketId);
            $scope.editTicket = function () {
                $window.location.href = "/overview/" + ticketId + "/edit";
            };

            TicketsService.currentUser()
            .then(
                function (data) {
                    $scope.user = data;
                },
                function (errResp) {
                    console.log(errResp);
                }
            )

            FeedbackService.getFeedbackByTicketId(ticketId)
            .then(
                function (data) {
                    if (data != "") {
                        $scope.feedback = data;
                    }
                }
            )

            $scope.leaveFeedback = function () {
                $window.location.href = "/overview/" + ticketId
                    + "/leavefeedback";
            };

            $scope.viewFeedback = function () {
                $window.location.href = "/overview/" + ticketId
                    + "/viewfeedback";
            };

            $scope.goBack = function () {
                $window.location.href = "/index";
            }

            var getHistory = function (ticketId) {
                TicketsService.getHistoryByTicketId(ticketId)
                .then(
                    function (response) {

                        var history = response;
                        angular.forEach(history, function (value, key) {
                            value.sortId = key;
                        });
                        $scope.histories = history;
                    },
                    function (errResponse) {
                        console.log(errResponse);
                    }
                );
            };

            var getComments = function (ticketId) {
                TicketsService.getCommentsByTicketId(ticketId)
                .then(
                    function (response) {
                        var comments = response;
                        angular.forEach(comments, function (value, key) {
                            value.sortId = key;
                        });
                        $scope.comments = comments;
                    },
                    function (errResponse) {
                        console.log(errResponse);
                    }
                )
            }

            var getAttachments = function (ticketId) {
                TicketsService.getAttachByTicketId(ticketId)
                .then(
                    function (data) {
                        $scope.attachments = data;
                    },
                    function (errResp) {
                        console.log(errResp);
                    }
                );
            }

            TicketsService.getTicket(ticketId)
            .then(
                function (data) {
                    $scope.ticket = data;
                    if (!$scope.ticket.approver) {
                        $scope.ticket.approver = Object.assign({},
                            $scope.ticket.owner);
                        $scope.ticket.approver.firstName = "-";
                        $scope.ticket.approver.lastName = "";
                    }
                    if (!$scope.ticket.assignee) {
                        $scope.ticket.assignee = Object.assign({},
                            $scope.ticket.owner);
                        $scope.ticket.assignee.firstName = "-";
                        $scope.ticket.assignee.lastName = "";
                    }
                    $scope.ticket.state = $scope.ticket.state.charAt(0)
                        + $scope.ticket.state.slice(1).toLowerCase();
                    $scope.ticket.urgency = $scope.ticket.urgency.charAt(0)
                        + $scope.ticket.urgency.slice(1).toLowerCase();
                    getAttachments($scope.ticket.id);
                    getHistory(ticketId);
                    getComments(ticketId);

                },
                function (errResp) {
                    console.log(errResp);
                }
            );
            $scope.showAllComments = false;
            $scope.showComments = function () {
                $scope.showAllComments = !$scope.showAllComments;
            }

            $scope.showAllHistory = false;
            $scope.showHistory = function () {
                $scope.showAllHistory = !$scope.showAllHistory;
            }

            $scope.saveNewComment = function () {
                var commentDto = {
                    text: $scope.comment,
                    ticketId: ticketId
                };
                TicketsService.saveNewComment(commentDto)
                .then(
                    function () {
                        $scope.comment = "";
                        getComments(ticketId);
                    },
                    function (errResp) {
                        console.log(errResp);
                    }
                )
            }

        }]);