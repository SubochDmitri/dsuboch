app.controller('TicketCtrl', ['$scope', '$http', '$window', 'TicketsService',
    function ($scope, $http, $window, TicketsService) {
        $scope.sortType     = 'id';
        $scope.sortReverse  = false;
        var user;
        TicketsService.currentUser()
        .then(
            function (data) {
                user = data;
                $scope.user = data;
                $scope.role = user.role;
                $scope.allTickets = user.allTickets;
                angular.forEach($scope.allTickets, function (value, key) {
                    if (value.state == 'IN_PROGRESS') {
                        value.state = 'In progress';
                    } else {
                        value.state = value.state.charAt(0) + value.state.slice(
                            1).toLowerCase();
                        value.urgency = value.urgency.charAt(0)
                            + value.urgency.slice(1).toLowerCase();
                    }

                });
                $scope.myTickets = user.myTickets;
                angular.forEach($scope.myTickets, function (value, key) {
                    if (value.state == 'IN_PROGRESS') {
                        value.state = 'In progress';
                    }
                    value.state = value.state.charAt(0) + value.state.slice(
                        1).toLowerCase();
                    value.urgency = value.urgency.charAt(0)
                        + value.urgency.slice(1).toLowerCase();

                });
            },
            function (errResp) {
                console.log(errResp);
            }
        )

        $scope.changeStatus = function (ticketId, state) {
            console.log(state);
            TicketsService.changeStatus(ticketId, state)
            .then(
                function () {
                    $window.location.reload();
                }
            )
        }
    }]);