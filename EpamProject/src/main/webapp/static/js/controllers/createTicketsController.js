app.controller('createTicketCtrl', ['$scope', '$http', '$window',
    function ($scope, $http, $window) {

        $http.get('/categories').success(function (data) {
            $scope.categories = data;
        });

        $scope.urgency = 'HIGH';
        $scope.category = '1';

        var saveTicket = function (state) {
            var categoryId = $scope.category;
            var name = $scope.name;
            var urgency = $scope.urgency;
            var description = $scope.description != undefined
                ? $scope.description : "-";
            var desiredResolutionDate = $scope.date ? new Date(
                $scope.date).getTime() : new Date().getTime();
            var comment = $scope.comment != undefined ? $scope.comment : "";

            var data = {
                name: name,
                state: state,
                category: {id: categoryId},
                urgency: urgency,
                comment: comment,
                description: description,
                desiredResolutionDate: desiredResolutionDate
            };
            console.log(data);
            var formData = new FormData();
            console.log($scope.file);

            var oFiles = document.getElementById("file").files,
                nFiles = oFiles.length;
            for (var nFileId = 0; nFileId < nFiles; nFileId++) {
                if (!exist(nFileId)) {
                    formData.append('files', oFiles[nFileId],
                        convertToEng(oFiles[nFileId].name));
                }
            }
            formData.append('ticket', JSON.stringify(data));
            var uploadUrl = "/tickets/";
            return $http({
                url: uploadUrl,
                method: 'POST',
                data: formData,
                headers: {'Content-Type': undefined},
                transformRequest: function (data, headersGetterFunction) {
                    return data;
                }
            })
            .then(
                function (response) {
                    $window.location.href = "/index";
                },
                function (errResponse) {
                    console.log(errResponse);
                    $scope.error = 'true';
                }
            )

        }

        $scope.saveDraftTicket = function () {
            saveTicket("DRAFT")
        }

        $scope.saveNewTicket = function () {
            saveTicket("NEW")
        }
    }]);