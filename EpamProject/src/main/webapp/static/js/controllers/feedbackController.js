app.controller('FeedbackCtrl',
    ['$http', '$window', '$scope', 'FeedbackService', 'TicketsService',
        function ($http, $window, $scope, FeedbackService, TicketsService) {
            var path = $window.location.pathname.split('/');
            var ticketId = path[path.length - 2];
            var rate = 0;

            TicketsService.getTicket(ticketId)
            .then(
                function (data) {
                    $scope.ticket = data;
                    console.log($scope.ticket);
                },
                function (errResp) {
                    console.log(errResp);
                }
            );
            $scope.selectRate = function (id) {
                rate = id;

            }

            $scope.saveFeedback = function () {
                console.log(rate);
                var data = {
                    ticket: $scope.ticket,
                    text: $scope.text,
                    rate: rate,
                    user: $scope.ticket.owner
                }
                console.log(data);
                FeedbackService.saveFeedback(data)
                .then(
                    function () {
                        window.history.back();
                    },
                    function (errResp) {
                        console.log(errResp);
                    }
                )
            }
        }]);