app.controller('editTicketCtrl',
    ['$scope', '$http', '$window', 'TicketsService',
        function ($scope, $http, $window, TicketsService) {

            var path = $window.location.pathname.split('/');
            var ticketId = path[path.length - 2];

            $http.get('/categories').success(function (data) {
                $scope.categories = data;
            })

            $scope.urgency = '';
            $scope.category = '';
            var attachs = function () {
                TicketsService.getAttachByTicketId(ticketId)
                .then(
                    function (data) {
                        $scope.attachments = data;
                    },
                    function (errResp) {
                        console.log(errResp);
                    }
                );
            }
            attachs();

            TicketsService.getTicket(ticketId)
            .then(
                function (data) {
                    $scope.ticket = data;
                }
            )

            var editTicket = function (state) {
                var categoryId = $scope.category ? $scope.category
                    : $scope.ticket.category.id;
                var name = $scope.name != undefined ? ($scope.name.trim() != ""
                    ? $scope.name : $scope.ticket.name) : $scope.ticket.name;
                var urgency = $scope.urgency ? $scope.urgency
                    : $scope.ticket.urgency;
                var description = $scope.description != undefined
                    ? ($scope.description.trim() != "" ? $scope.description
                        : $scope.ticket.description)
                    : $scope.ticket.description;
                var desiredResolutionDate = $scope.date ? new Date(
                    $scope.date).getTime() : new Date().getTime();
                var comment = $scope.comment != undefined ? $scope.comment : "";

                var data = {
                    id: $scope.ticket.id,
                    name: name,
                    state: state,
                    category: {id: categoryId},
                    urgency: urgency,
                    comment: comment,
                    description: description,
                    desiredResolutionDate: desiredResolutionDate
                };
                console.log(data);
                var formData = new FormData();
                console.log($scope.file);

                var oFiles = document.getElementById("file").files,
                    nFiles = oFiles.length;
                for (var nFileId = 0; nFileId < nFiles; nFileId++) {
                    if (!exist(nFileId)) {
                        formData.append('files', oFiles[nFileId],
                            convertToEng(oFiles[nFileId].name));
                    }
                }
                formData.append('ticket', JSON.stringify(data));
                var uploadUrl = "/tickets/";
                return $http({
                    url: uploadUrl,
                    method: 'PUT',
                    data: formData,
                    headers: {'Content-Type': undefined},
                    transformRequest: function (data, headersGetterFunction) {
                        return data;
                    }
                })
                .then(
                    function () {
                        $window.location.href = "/overview/" + ticketId;
                    },
                    function (errResponse) {
                        console.log(errResponse);
                        $scope.error = 'true';
                    }
                )

            }
            $scope.deleteAttach = function () {

                window.onclick = function (e) {
                    var elem = e ? e.target : window.event.srcElement;
                    TicketsService.deleteAttachById(elem.alt)
                    .then(
                        function () {
                            attachs();
                        }
                    ),
                        function (errResponse) {
                            console.log(errResponse);
                        }
                }

            }

            $scope.saveDraftTicket = function () {
                editTicket("DRAFT")
            }

            $scope.saveNewTicket = function () {
                editTicket("NEW")
            }
        }]);