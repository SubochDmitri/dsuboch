app.service('TicketsService', ['$http', function ($http) {

    this.saveNewComment = function (text) {
        return $http.post("/comment/", text)
        .then(
            function (response) {
                return response.data
            },
            function (errResponse) {
                return errResponse.statusText;
            }
        )
    };

    this.currentUser = function () {
        return $http.get('/users/current')
        .then(
            function (response) {
                return response.data;
            },
            function (errResp) {
                return errResp;
            }
        )
    }

    this.getCommentsByTicketId = function (ticketId) {
        return $http.get("/comment/" + ticketId)
        .then(
            function (response) {
                console.log(response);
                return response.data;
            },
            function (errResponse) {
                return errResponse.statusText;
            }
        )
    };

    this.getTicket = function (ticketId) {
        return $http.get("/tickets/overview/get/" + ticketId)
        .then(
            function (response) {
                return response.data;
            },
            function (errResponse) {
                return errResponse.statusText;
            }
        )
    };

    this.getAttachByTicketId = function (ticketId) {
        return $http.get("/attachment/" + ticketId)
        .then(
            function (response) {
                return response.data;
            },
            function (errResponse) {
                return errResponse.statusText;
            }
        )
    };

    this.deleteAttachById = function (attachId) {
        return $http.delete("/attachment/" + attachId)
        .then(
            function (response) {
                return response.data;
            },
            function (errResponse) {
                return errResponse.statusText;
            }
        )
    };

    this.getHistoryByTicketId = function (ticketId) {
        return $http.get("/history/" + ticketId)
        .then(
            function (response) {
                return response.data;
            },
            function (errResponse) {
                return errResponse.statusText;
            }
        )
    };

    this.changeStatus = function (ticketId, state) {
        return $http.put("/tickets/action/" + ticketId, state)
        .then(
            function (response) {
                return response.data
            },
            function (errResponse) {
                return errResponse.statusText;
            }
        )
    };

}]);