app.service('FeedbackService', ['$http', function ($http) {

    this.getFeedbackByTicketId = function (ticketId) {
        return $http.get("/feedback/" + ticketId)
        .then(
            function (response) {
                return response.data;
            },
            function (errResponse) {
                return errResponse.statusText;
            }
        )
    };

    this.saveFeedback = function (feedback) {
        return $http.post("/feedback/", feedback)
        .then(
            function (response) {
                return response.data
            },
            function (errResponse) {
                return errResponse.statusText;
            }
        )
    };
}]);