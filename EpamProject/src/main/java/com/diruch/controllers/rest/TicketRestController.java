package com.diruch.controllers.rest;

import com.diruch.converters.TicketConverter;
import com.diruch.dto.TicketDto;
import com.diruch.enums.State;
import com.diruch.exceptions.AccessErrorException;
import com.diruch.exceptions.InvalidDataException;
import com.diruch.exceptions.InvalidFileException;
import com.diruch.exceptions.InvalidJsonException;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.services.TicketService;
import com.diruch.services.UserService;
import java.io.IOException;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/tickets")
public class TicketRestController {

    @Autowired
    private UserService userService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private TicketConverter ticketConverter;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void saveTicket(@RequestParam(value = "files") MultipartFile[] files,
        Authentication authentication, @RequestParam(value = "ticket") String ticket)
        throws IOException, InvalidFileException, InvalidJsonException, MessagingException, InvalidDataException {
        ticketService.createTicket(authentication.getName(), ticket, files);
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public void updateTicket(@RequestParam(value = "files") MultipartFile[] files,
        Authentication authentication, @RequestParam(value = "ticket") String ticket)
        throws IOException, InvalidFileException, InvalidJsonException, MessagingException, InvalidDataException {
        ticketService.updateTicket(authentication.getName(), ticket, files);
    }

    @RequestMapping(value = "/overview/get/{ticketid}")
    public TicketDto getTicketById(@PathVariable int ticketid) {
        return ticketConverter.convertToDto(ticketService.getTicketById(ticketid));
    }

    @RequestMapping(value = "/action/{ticketid}", method = RequestMethod.PUT)
    public void changeStatus(@PathVariable int ticketid, @RequestBody String state,
        Authentication authentication) throws MessagingException, AccessErrorException {
        State changeState = State.valueOf(state);
        Ticket ticket = ticketService.getTicketById(ticketid);
        User user = userService.loadUserByUsername(authentication.getName());
        switch (changeState) {
            case APPROVED:
                ticketService.approve(ticket, user);
                break;
            case NEW:
                ticketService.submit(ticket, user);
                break;
            case DECLINED:
                ticketService.decline(ticket, user);
                break;
            case CANCELED:
                ticketService.cancel(ticket, user);
                break;
            case IN_PROGRESS:
                ticketService.assign(ticket, user);
                break;
            case DONE:
                ticketService.done(ticket, user);
                break;
        }
    }
}
