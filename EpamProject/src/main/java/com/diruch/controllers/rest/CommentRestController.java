package com.diruch.controllers.rest;

import com.diruch.dto.CommentDto;
import com.diruch.exceptions.InvalidDataException;
import com.diruch.services.CommentService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/comment")
public class CommentRestController {

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/{ticketid}", method = RequestMethod.GET)
    public List<CommentDto> getCommentByTicket(@PathVariable int ticketid) {
        return commentService.getCommentsByTicketId(ticketid);
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void saveComment(@RequestBody @Valid CommentDto commentDto,
        Authentication authentication) throws InvalidDataException {
        commentService.saveCommentByUser(commentDto, authentication.getName());
    }
}
