package com.diruch.controllers.rest;

import com.diruch.models.Attachment;
import com.diruch.services.AttachmentService;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/attachment")
public class AttachmentRestController {

    @Autowired
    private AttachmentService attachmentService;

    @RequestMapping(value = "/{ticketId}")
    public List<Attachment> getAttachmentById(@PathVariable int ticketId) {
        return attachmentService.getAttachmentsByTicketId(ticketId);
    }

    @RequestMapping(value = "/{attachId}", method = RequestMethod.DELETE)
    public void deleteAttachmentById(@PathVariable int attachId, Authentication authentication) {
        attachmentService.delete(attachId, authentication.getName());
    }

    @RequestMapping(value = "/download/{attachId}")
    public void downloadAttachment(@PathVariable int attachId, HttpServletResponse response)
        throws IOException {
        Attachment attachment = attachmentService.getAttachmentById(attachId);
        response.setContentType(attachment.getContentType());
        String nameFile = attachment.getName();
        File file = new File(nameFile);
        FileOutputStream fos = new FileOutputStream(nameFile);
        fos.write(attachment.getBlob());
        fos.flush();
        response.setHeader("Content-Disposition",
            String.format("inline; filename=\"" + nameFile + "\""));
        response.setContentLength((int) file.length());
        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
        FileCopyUtils.copy(inputStream, response.getOutputStream());
        file.delete();
    }
}
