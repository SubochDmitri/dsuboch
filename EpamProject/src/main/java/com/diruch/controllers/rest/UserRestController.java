package com.diruch.controllers.rest;

import com.diruch.dto.UserDto;
import com.diruch.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
public class UserRestController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public UserDto getCurrentUser(Authentication authentication) {
        return userService.getCurrentUser(authentication.getName());
    }
}
