package com.diruch.controllers.rest;

import com.diruch.dto.HistoryDto;
import com.diruch.services.HistoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/history")
public class HistoryRestController {

    @Autowired
    private HistoryService historyService;

    @RequestMapping(value = "/{ticketid}")
    public List<HistoryDto> getHistoryByTicketId(@PathVariable int ticketid) {
        return historyService.getHistoryByTicketId(ticketid);
    }
}
