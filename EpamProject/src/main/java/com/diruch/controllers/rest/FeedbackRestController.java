package com.diruch.controllers.rest;

import com.diruch.exceptions.AccessErrorException;
import com.diruch.models.Feedback;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.services.FeedbackService;
import com.diruch.services.TicketService;
import com.diruch.services.UserService;
import com.diruch.validator.TicketValidator;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/feedback")
public class FeedbackRestController {

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private TicketValidator validator;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/{ticketid}", method = RequestMethod.GET)
    public Feedback getFeedbackByTicketid(@PathVariable int ticketid) throws AccessErrorException {
        Ticket ticket = ticketService.getTicketById(ticketid);
        if (validator.isValidForViewFeedback(ticket)) {
            return feedbackService.getFeedbackByTicketId(ticketid);
        } else {
            throw new AccessErrorException("Can't view a feedback");
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void saveFeedback(@RequestBody Feedback feedback, Authentication authentication)
        throws MessagingException, AccessErrorException {
        Ticket ticket = feedback.getTicket();
        User user = userService.loadUserByUsername(authentication.getName());
        if (validator.isValidForLeaveFeedback(ticket, user)) {
            feedbackService.save(feedback);
        } else {
            throw new AccessErrorException("Can't leave a feedback!");
        }
    }
}
