package com.diruch.controllers;

import javax.mail.MessagingException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TicketController {

    @RequestMapping(value = {"/index", "/"})
    public String getIndexPage() throws MessagingException {
        return "tickets";
    }

    @RequestMapping(value = "/newticket")
    public String getNewTicketPage() {
        return "create-ticket";
    }

    @RequestMapping(value = "overview/{ticketid}")
    public String getOverviewPage() {
        return "ticket-overview";
    }

    @RequestMapping(value = "overview/{ticketid}/edit")
    public String getEditPage() {
        return "edit-ticket";
    }

    @RequestMapping(value = "overview/{ticketid}/leavefeedback")
    public String getLeaveFeedbackPage() {
        return "leave-feedback";
    }

    @RequestMapping(value = "overview/{ticketid}/viewfeedback")
    public String getViewFeedbackPage() {
        return "view-feedback";
    }
}
