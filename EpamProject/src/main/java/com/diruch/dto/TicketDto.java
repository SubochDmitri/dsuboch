package com.diruch.dto;

import com.diruch.enums.State;
import com.diruch.enums.Urgency;
import com.diruch.models.Category;
import com.diruch.models.Feedback;
import com.diruch.models.User;
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TicketDto {

    private Integer id;
    private String name;
    private String description;
    private Date createdOn;
    private Date desiredResolutionDate;
    private User assignee;
    private User owner;
    private State state;
    private Category category;
    private Urgency urgency;
    private User approver;
    private String comment;
    private Feedback feedback;
}
