package com.diruch.dto;

import com.diruch.enums.Role;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Integer id;
    private Role role;
    private String email;
    private List<TicketDto> allTickets;
    private List<TicketDto> myTickets;


}
