package com.diruch.dto;

import com.diruch.models.Ticket;
import com.diruch.models.User;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentDto {

    private int id;
    private Ticket ticket;
    private int ticketId;
    private Timestamp date;
    private String text;
    private User user;


}
