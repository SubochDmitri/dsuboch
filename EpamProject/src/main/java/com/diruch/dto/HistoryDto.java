package com.diruch.dto;

import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HistoryDto {

    private Timestamp date;
    private String action;
    private String userFirstName;
    private String userLastName;
    private String description;
}
