package com.diruch.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
@EnableWebSecurity
@ComponentScan(value = "com.diruch")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("CustomUserDetailsService")
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/static/**").permitAll()
            .antMatchers(HttpMethod.GET, "/newticket").hasAnyAuthority("MANAGER", "EMPLOYEE")
            .antMatchers(HttpMethod.DELETE, "/attachment//{attachId}").hasAnyAuthority("MANAGER", "EMPLOYEE")
            .antMatchers(HttpMethod.POST, "/feedback/").hasAnyAuthority("MANAGER", "EMPLOYEE")
            .antMatchers(HttpMethod.POST, "/tickets/").hasAnyAuthority("MANAGER", "EMPLOYEE")
            .antMatchers(HttpMethod.PUT, "/tickets/").hasAnyAuthority("MANAGER", "EMPLOYEE")
            .anyRequest().authenticated()
            .and()
            .formLogin()
            .permitAll()
            .loginPage("/login")
            .failureUrl("/login-error")
            .usernameParameter("email")
            .passwordParameter("password")
            .permitAll()
            .defaultSuccessUrl("/index", true);

    }
}
