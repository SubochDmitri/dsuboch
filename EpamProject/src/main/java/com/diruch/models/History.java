package com.diruch.models;

import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
@Table(name = "history")
public class History {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "ticket_id")
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Ticket ticket;

    @Column(name = "date")
    @NotNull
    private Timestamp date = new Timestamp(new Date().getTime());

    @Column(name = "action")
    @NotNull
    private String action;

    @JoinColumn(name = "user_id")
    @NotNull
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private User user;

    @Column(name = "description")
    @NotNull
    private String description;
}
