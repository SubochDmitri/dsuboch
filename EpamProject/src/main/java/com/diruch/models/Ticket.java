package com.diruch.models;

import com.diruch.enums.State;
import com.diruch.enums.Urgency;
import java.sql.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @NotNull
    @Size(max = 100)
    @Column(name = "name")
    private String name;

    @Size(max = 500)
    @Column(name = "description")
    private String description;

    @Column(name = "created_on")
    private Date createdOn = new Date(new java.util.Date().getTime());

    @Column(name = "desired_resolution_date")
    private Date desiredResolutionDate = new Date(new java.util.Date().getTime());

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "assignee_id")
    private User assignee;

    @NotNull
    @JoinColumn(name = "owner_id")
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private User owner;

    @Column(name = "state_id")
    @Enumerated(EnumType.ORDINAL)
    private State state;

    @NotNull
    @JoinColumn(name = "category_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Category category;

    @NotNull
    @Column(name = "urgency_id")
    @Enumerated(EnumType.ORDINAL)
    private Urgency urgency;

    @JoinColumn(name = "approver_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private User approver;


    public Ticket() {
    }

    public Ticket(@NotNull @Size(max = 100) String name,
        @NotNull @Size(max = 100) String description, Date createdOn, Date desiredResolutionDate,
        User assignee, @NotNull User owner, State state, @NotNull Category category,
        @NotNull Urgency urgency, User approver) {
        this.name = name;
        this.description = description;
        this.createdOn = createdOn;
        this.desiredResolutionDate = desiredResolutionDate;
        this.assignee = assignee;
        this.owner = owner;
        this.state = state;
        this.category = category;
        this.urgency = urgency;
        this.approver = approver;
    }
}