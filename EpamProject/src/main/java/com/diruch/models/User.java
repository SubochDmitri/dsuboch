package com.diruch.models;

import com.diruch.enums.Role;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    @NotNull
    @Size(max = 25)
    private String firstName;

    @Column(name = "last_name")
    @NotNull
    @Size(max = 25)
    private String lastName;

    @Column(name = "position")
    @NotNull
    @Size(max = 30)
    private String position;

    @Column(name = "phone")
    @NotNull
    @Size(max = 20)
    private String phone;

    @Column(name = "role_id")
    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private Role role;

    @Column(name = "email", unique = true)
    @NotNull
    @Size(max = 100)
    private String email;

    @Column(name = "password")
    @NotNull
    @Size(max = 20)
    private String password;

    @Column(name = "adress")
    @NotNull
    @Size(max = 100)
    private String adress;
}