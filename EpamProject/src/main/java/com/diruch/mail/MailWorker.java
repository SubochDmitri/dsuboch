package com.diruch.mail;

import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:mail.properties")
public class MailWorker {

    @Autowired
    private Environment environment;

    private JavaMailSender sender() {
        final JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setUsername(environment.getRequiredProperty("email"));
        sender.setPassword(environment.getRequiredProperty("password"));
        sender.setHost(environment.getRequiredProperty("host"));
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");
        sender.setJavaMailProperties(props);
        return sender;
    }


    public void sendMail(String[] to, String subject, String msg) throws MessagingException {
        for (String t : to) {
            JavaMailSender sender = sender();
            MimeMessage mimeMsg = sender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMsg, true);
            helper.setFrom(environment.getRequiredProperty("email"));
            helper.setTo(t);
            helper.setSubject(subject);
            helper.setText(msg, true);
            sender.send(mimeMsg);
        }
    }
}
