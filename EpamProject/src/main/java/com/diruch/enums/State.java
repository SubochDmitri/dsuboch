package com.diruch.enums;

public enum State {
    DRAFT, NEW, APPROVED, DECLINED, IN_PROGRESS, DONE, CANCELED

}
