package com.diruch.enums;

public enum Urgency {
    CRITICAL, HIGH, MEDIUM, LOW
}
