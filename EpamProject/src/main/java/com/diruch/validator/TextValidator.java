package com.diruch.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class TextValidator {

    public boolean isValid(String string) {
        Pattern pattern = Pattern
            .compile("[a-zA-Z0-9~.x22(),:;<>@\\[\\]!#$%&x27*+-=/?^_`{|}]{0,500}");
        Matcher matcher = pattern.matcher(string);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    public boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[a-z0-9~.x22(),:;<>@\\[\\]!#$%&x27*+-=/?^_`{|}]{0,100}");
        Matcher matcher = pattern.matcher(name);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }
}
