package com.diruch.validator;

import com.diruch.enums.Role;
import com.diruch.enums.State;
import com.diruch.models.Feedback;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.services.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TicketValidator {

    @Autowired
    private FeedbackService feedbackService;

    public boolean isValidForApproveOrDecline(Ticket ticket, User user) {
        if (State.NEW == ticket.getState() &&
            ticket.getOwner().getId() != user.getId()) {
            return true;
        }
        return false;
    }

    public boolean isValidForCancel(Ticket ticket, User user) {
        boolean isEmployeeAbleToCancel =
            (State.DRAFT == ticket.getState() || State.DECLINED == ticket.getState()) &&
                Role.EMPLOYEE == user.getRole();
        boolean isEngineerAbleToCancel = State.APPROVED == ticket.getState() &&
            Role.ENGINEER == user.getRole();
        boolean isManagerAbleToCancel =
            ((State.DRAFT == ticket.getState() || State.DECLINED == ticket.getState()) &&
                Role.MANAGER == user.getRole() &&
                ticket.getOwner().getId() == user.getId())
                || (State.NEW == ticket.getState() &&
                Role.MANAGER == user.getRole() &&
                ticket.getOwner().getId() != user.getId());

        if (isEmployeeAbleToCancel || isManagerAbleToCancel || isEngineerAbleToCancel) {
            return true;
        }
        return false;
    }

    public boolean isValidForSubmit(Ticket ticket, User user) {
        boolean isEmployeeAbleToSubmit =
            (State.DRAFT == ticket.getState() || State.DECLINED == ticket.getState()) &&
                Role.EMPLOYEE == user.getRole();
        boolean isManagerAbleToSubmit =
            ((State.DRAFT == ticket.getState() || State.DECLINED == ticket.getState())) &&
                Role.MANAGER == user.getRole() && ticket.getOwner().getId() == user.getId();
        if (isEmployeeAbleToSubmit || isManagerAbleToSubmit) {
            return true;
        }
        return false;
    }

    public boolean isValidForAssign(Ticket ticket) {
        if (State.APPROVED == ticket.getState()) {
            return true;
        }
        return false;
    }

    public boolean isValidForDone(Ticket ticket, User user) {
        if (State.IN_PROGRESS == ticket.getState() &&
            ticket.getAssignee().getId() == user.getId()) {
            return true;
        }
        return false;
    }


    public boolean isValidForLeaveFeedback(Ticket ticket, User user) {
        Feedback feedback = feedbackService.getFeedbackByTicketId(ticket.getId());
        if (!(ticket.getState() == State.DONE) || !(ticket.getOwner().getId() == user.getId())
            || feedback != null) {
            return false;
        }
        return true;
    }

    public boolean isValidForViewFeedback(Ticket ticket) {
        if (ticket.getState() != State.DONE) {
            return false;
        }
        return true;
    }
}
