package com.diruch.services;

import com.diruch.exceptions.AccessErrorException;
import com.diruch.exceptions.InvalidDataException;
import com.diruch.exceptions.InvalidFileException;
import com.diruch.exceptions.InvalidJsonException;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import java.io.IOException;
import javax.mail.MessagingException;
import org.springframework.web.multipart.MultipartFile;

public interface TicketService {

    void save(Ticket ticket);

    void createTicket(String username, String ticketJSON, MultipartFile[] files)
        throws IOException, InvalidJsonException, InvalidFileException, MessagingException, InvalidDataException;

    void updateTicket(String username, String ticketJSON, MultipartFile[] files)
        throws IOException, InvalidJsonException, InvalidFileException, MessagingException, InvalidDataException;

    Ticket getTicketById(int ticketId);

    void delete(Ticket ticket);

    void update(Ticket ticket);

    void submit(Ticket ticket, User user) throws MessagingException, AccessErrorException;

    void cancel(Ticket ticket, User user) throws MessagingException, AccessErrorException;

    void approve(Ticket ticket, User user) throws MessagingException, AccessErrorException;

    void decline(Ticket ticket, User user) throws MessagingException, AccessErrorException;

    void assign(Ticket ticket, User user) throws AccessErrorException;

    void done(Ticket ticket, User user) throws MessagingException, AccessErrorException;
}
