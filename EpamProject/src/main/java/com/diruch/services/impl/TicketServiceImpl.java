package com.diruch.services.impl;

import com.diruch.converters.TicketConverter;
import com.diruch.dto.TicketDto;
import com.diruch.enums.State;
import com.diruch.exceptions.AccessErrorException;
import com.diruch.exceptions.InvalidDataException;
import com.diruch.exceptions.InvalidFileException;
import com.diruch.exceptions.InvalidJsonException;
import com.diruch.models.Category;
import com.diruch.models.Comment;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.repository.TicketRepository;
import com.diruch.services.AttachmentService;
import com.diruch.services.CategoryService;
import com.diruch.services.CommentService;
import com.diruch.services.HistoryService;
import com.diruch.services.MailService;
import com.diruch.services.TicketService;
import com.diruch.services.UserService;
import com.diruch.validator.TicketValidator;
import java.io.IOException;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private UserService userService;

    @Autowired
    private TicketConverter ticketConverter;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttachmentService attachmentService;

    @Autowired
    private TicketValidator validator;

    @Autowired
    private CommentService commentService;

    @Autowired
    private MailService mailService;

    @Override
    public void save(Ticket ticket) {
        ticketRepository.save(ticket);
    }

    @Override
    public Ticket getTicketById(int ticketId) {
        return ticketRepository.getTicketById(ticketId);
    }

    @Override
    public void delete(Ticket ticket) {
        ticketRepository.delete(ticket);
    }

    @Override
    public void update(Ticket ticket) {
        ticketRepository.update(ticket);
    }

    @Override
    public void submit(Ticket ticket, User user) throws MessagingException, AccessErrorException {
        if (validator.isValidForSubmit(ticket, user)) {
            historyService.historyChangeStatus(ticket, user, State.NEW);
            mailService.sendTicketsForApprove(ticket);
            ticketRepository.submit(ticket, user);
        } else {
            throw new AccessErrorException("Can't submit a ticket");
        }
    }

    @Override
    public void cancel(Ticket ticket, User user) throws MessagingException, AccessErrorException {
        if (validator.isValidForCancel(ticket, user)) {
            historyService.historyChangeStatus(ticket, user, State.CANCELED);
            mailService.sendTicketWasCancelled(ticket, user);
            ticketRepository.cancel(ticket, user);
        } else {
            throw new AccessErrorException("Can't cancel a ticket");
        }
    }

    @Override
    public void approve(Ticket ticket, User user) throws MessagingException, AccessErrorException {
        if (validator.isValidForApproveOrDecline(ticket, user)) {
            historyService.historyChangeStatus(ticket, user, State.APPROVED);
            mailService.sendTicketWasApprove(ticket);
            ticketRepository.approve(ticket, user);
        } else {
            throw new AccessErrorException("Can't approve a ticket");
        }
    }

    @Override
    public void decline(Ticket ticket, User user) throws MessagingException, AccessErrorException {
        if (validator.isValidForApproveOrDecline(ticket, user)) {
            historyService.historyChangeStatus(ticket, user, State.DECLINED);
            mailService.sendTicketWasDeclined(ticket);
            ticketRepository.decline(ticket, user);
        }
        throw new AccessErrorException("Can't decline a ticket");
    }

    @Override
    public void assign(Ticket ticket, User user) throws AccessErrorException {
        if (validator.isValidForAssign(ticket)) {
            historyService.historyChangeStatus(ticket, user, State.IN_PROGRESS);
            ticketRepository.assign(ticket, user);
        } else {
            throw new AccessErrorException("Can't assign a ticket");
        }
    }

    @Override
    public void done(Ticket ticket, User user) throws MessagingException, AccessErrorException {
        if (validator.isValidForDone(ticket, user)) {
            historyService.historyChangeStatus(ticket, user, State.DONE);
            mailService.sendTicketWasDone(ticket);
            ticketRepository.done(ticket);
        } else {
            throw new AccessErrorException("Can't done a ticket");
        }
    }

    @Override
    public void updateTicket(String username, String ticketJSON, MultipartFile[] files)
        throws IOException, InvalidJsonException, InvalidFileException, MessagingException, InvalidDataException {
        TicketDto ticketDto = null;
        if (!ticketJSON.isEmpty()) {
            ticketDto = ticketConverter.convertFromJSON(ticketJSON);
        } else {
            throw new InvalidJsonException("Wrong JSON");
        }
        User user = userService.loadUserByUsername(username);
        Category category = categoryService.getCategoryById(ticketDto.getCategory().getId());
        Ticket ticket = ticketConverter.convertFromDto(ticketDto);
        ticket.setOwner(user);
        ticket.setCategory(category);
        mailService.sendTicketsForApprove(ticket);
        update(ticket);
        historyService.historyEditTicket(ticket, user);
        attachmentService.saveAttachments(files, ticket.getId(), username);
        saveComments(ticketDto, user, ticket);
    }


    @Override
    public void createTicket(String username, String ticketJSON, MultipartFile[] files)
        throws IOException, InvalidJsonException, InvalidFileException, MessagingException, InvalidDataException {
        TicketDto ticketDto = null;
        if (!ticketJSON.isEmpty()) {
            ticketDto = ticketConverter.convertFromJSON(ticketJSON);
        } else {
            throw new InvalidJsonException("Wrong JSON");
        }
        User user = userService.loadUserByUsername(username);
        Category category = categoryService.getCategoryById(ticketDto.getCategory().getId());
        Ticket ticket = ticketConverter.convertFromDto(ticketDto);
        ticket.setOwner(user);
        ticket.setCategory(category);
        save(ticket);
        mailService.sendTicketsForApprove(ticket);
        historyService.historyCreateTicket(ticket, user);
        attachmentService.saveAttachments(files, ticket.getId(), username);
        saveComments(ticketDto, user, ticket);
    }


    private void saveComments(TicketDto ticketDto, User user, Ticket ticket) {
        if (!ticketDto.getComment().equals("") && ticketDto.getComment() != null) {
            Comment comment = new Comment();
            comment.setTicket(ticket);
            comment.setUser(user);
            comment.setText(ticketDto.getComment());
            commentService.save(comment);
        }
    }

}
