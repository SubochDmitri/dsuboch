package com.diruch.services.impl;

import com.diruch.converters.HistoryConverter;
import com.diruch.dto.HistoryDto;
import com.diruch.enums.State;
import com.diruch.models.History;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.repository.HistoryRepository;
import com.diruch.services.HistoryService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private HistoryRepository historyRepository;

    @Autowired
    private HistoryConverter historyConverter;

    @Override
    public void save(History history) {
        historyRepository.save(history);
    }

    @Override
    public List<HistoryDto> getHistoryByTicketId(int ticketId) {
        List<History> list = historyRepository.getHistoryByTicketId(ticketId);
        List<HistoryDto> historyDtos = new ArrayList<>();
        for (History history : list) {
            historyDtos.add(historyConverter.convertToDto(history));
        }
        return historyDtos;
    }

    @Override
    public void historyCreateTicket(Ticket ticket, User user) {
        History history = new History();
        history.setTicket(ticket);
        history.setUser(user);
        history.setAction("Ticket is created");
        history.setDescription("Ticket is created");
        save(history);
    }

    @Override
    public void historyEditTicket(Ticket ticket, User user) {
        History history = new History();
        history.setTicket(ticket);
        history.setUser(user);
        history.setAction("Ticket is edited");
        history.setDescription("Ticket is edited");
        save(history);
    }

    @Override
    public void historyChangeStatus(Ticket ticket, User user, State to) {
        History history = new History();
        history.setTicket(ticket);
        history.setUser(user);
        history.setAction("Ticket status is changed");
        history.setDescription(
            "Ticket status is changed from '" + ticket.getState() + "' to '" + to + "'");
        save(history);
    }

    @Override
    public void historyAttachFile(Ticket ticket, User user, String fileName) {
        History history = new History();
        history.setTicket(ticket);
        history.setUser(user);
        history.setAction("File is attached");
        history.setDescription("File is attached '" + fileName + "'");
        save(history);
    }

    @Override
    public void historyRemoveFile(Ticket ticket, User user, String fileName) {
        History history = new History();
        history.setTicket(ticket);
        history.setUser(user);
        history.setAction("File is removed");
        history.setDescription("File is removed '" + fileName + "'");
        save(history);
    }
}
