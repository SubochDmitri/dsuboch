package com.diruch.services.impl;

import com.diruch.converters.CommentConverter;
import com.diruch.dto.CommentDto;
import com.diruch.exceptions.InvalidDataException;
import com.diruch.models.Comment;
import com.diruch.repository.CommentRepository;
import com.diruch.services.CommentService;
import com.diruch.services.TicketService;
import com.diruch.services.UserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private CommentConverter commentConverter;

    @Override
    public void save(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public List<CommentDto> getCommentsByTicketId(int ticketId) {
        int i;
        i=1;
        List<CommentDto> commentDtos = new ArrayList<>();
        List<Comment> comments = commentRepository.getCommentsByTicketId(ticketId);
        for (Comment comment : comments) {
            commentDtos.add(commentConverter.convertToDto(comment));
        }
        return commentDtos;
    }

    @Override
    public void saveCommentByUser(CommentDto commentDto, String username)
        throws InvalidDataException {
        commentDto.setTicket(ticketService.getTicketById(commentDto.getTicketId()));
        commentDto.setUser(userService.loadUserByUsername(username));
        Comment comment = commentConverter.convertFromDto(commentDto);
        save(comment);
    }


}
