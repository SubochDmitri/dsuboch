package com.diruch.services.impl;

import com.diruch.converters.TicketConverter;
import com.diruch.converters.UserConverter;
import com.diruch.dto.TicketDto;
import com.diruch.dto.UserDto;
import com.diruch.enums.Role;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.repository.UserRepository;
import com.diruch.services.UserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private TicketConverter ticketConverter;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserConverter userConverter;

    @Override
    public User loadUserByUsername(String username) {
        return userRepository.loadUserByUsername(username);
    }



    @Override
    public List<TicketDto> getManagerTicketsDto(User user) {
        return ticketConverter.convertListInDto(userRepository.getManagerTickets(user));
    }

    @Override
    public List<TicketDto> getOwnerTicketsDto(User user) {
        return ticketConverter.convertListInDto(userRepository.getOwnerTickets(user));
    }

    @Override
    public List<TicketDto> getEngineerTicketsDto(User user) {
        return ticketConverter.convertListInDto(userRepository.getEngineerTickets(user));
    }

    @Override
    public List<TicketDto> getAcceptedTickets(User user) {
        return ticketConverter.convertListInDto(userRepository.getAcceptedTickets(user));
    }

    @Override
    public UserDto getCurrentUser(String username) {
        List<TicketDto> allTickets = null;
        User user = loadUserByUsername(username);
        List<TicketDto> myTickets = getOwnerTicketsDto(user);
        boolean employee = user.getRole().toString().toLowerCase()
            .equals(Role.EMPLOYEE.toString().toLowerCase());
        boolean manager = user.getRole().toString().toLowerCase()
            .equals(Role.MANAGER.toString().toLowerCase());
        boolean engineer = user.getRole().toString().toLowerCase()
            .equals(Role.ENGINEER.toString().toLowerCase());
        if (employee) {
            allTickets = getOwnerTicketsDto(user);
        }
        if (manager) {
            allTickets = getManagerTicketsDto(user);
        }
        if (engineer) {
            allTickets = getEngineerTicketsDto(user);
            myTickets = getAcceptedTickets(user);
        }
        return userConverter.convertToDto(user, allTickets, myTickets);
    }

    @Override
    public List<User> getAllManagers() {
        return userRepository.getAllManagers();
    }

    @Override
    public List<User> getAllEngineers() {
        return userRepository.getAllEngineers();
    }

}
