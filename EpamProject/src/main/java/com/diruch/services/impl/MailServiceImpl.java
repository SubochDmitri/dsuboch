package com.diruch.services.impl;

import com.diruch.enums.Role;
import com.diruch.enums.State;
import com.diruch.mail.MailWorker;
import com.diruch.models.Feedback;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.services.MailService;
import com.diruch.services.UserService;
import java.util.ArrayList;
import java.util.List;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private MailWorker mailWorker;

    @Autowired
    private UserService userService;

    @Override
    public void sendTicketsForApprove(Ticket ticket) throws MessagingException {
        if (ticket.getState() == State.NEW) {
            List<User> users = userService.getAllManagers();
            String[] to = new String[users.size()];
            int i = 0;
            for (User user : users) {
                to[i] = user.getEmail();
                i++;
            }
            String subj = "New ticket for approval";
            String message = "Dear Managers,<br><br>\n" +
                "\n" +
                "New ticket <a href='http://localhost:8081/overview/" + ticket.getId() + "'>"
                + ticket.getName() + "</a> waiting for your approval";
            mailWorker.sendMail(to, subj, message);
        }
    }

    @Override
    public void sendTicketWasApprove(Ticket ticket) throws MessagingException {
        List<User> users = userService.getAllEngineers();
        String[] to = new String[users.size() + 1];
        int i = 0;
        for (User us : users) {
            to[i] = us.getEmail();
            i++;
        }
        to[i] = ticket.getOwner().getEmail();
        String subj = "Ticket was approved";
        String message = "Dear Users,<br><br>\n" +
            "\n" +
            "New ticket <a href='http://localhost:8081/overview/" + ticket.getId() + "'>"
            + ticket
            .getName() + "</a> was approved by the Manager";
        mailWorker.sendMail(to, subj, message);
    }

    @Override
    public void sendTicketWasDeclined(Ticket ticket) throws MessagingException {
        String[] to = {ticket.getOwner().getEmail()};
        String subj = "Ticket was declined";
        String message =
            "Dear " + ticket.getOwner().getFirstName() + " " + ticket.getOwner().getLastName()
                + "<br><br>\n" +
                "\n" +
                "Ticket <a href='http://localhost:8081/overview/" + ticket.getId() + "'>"
                + ticket
                .getName() + "</a> was declined by the Manager";
        mailWorker.sendMail(to, subj, message);
    }

    @Override
    public void sendTicketWasCancelled(Ticket ticket, User user) throws MessagingException {
        List<String> emails = new ArrayList<>();
        emails.add(ticket.getOwner().getEmail());
        String[] to = null;
        String subj = "Ticket was cancelled";
        String message =
            "Dear " + ticket.getOwner().getFirstName() + " " + ticket.getOwner().getLastName()
                + "<br><br>\n" +
                "\n" +
                "Ticket <a href='http://localhost:8081/overview/" + ticket.getId() + "'>"
                + ticket
                .getName() + "</a> was cancelled";
        if (user.getRole() == Role.ENGINEER) {
            message = "Dear Users,<br><br>\n" +
                "\n" +
                "Ticket <a href='http://localhost:8081/overview/" + ticket.getId() + "'>"
                + ticket
                .getName() + "</a> was cancelled by the Engineer";
            emails.add(ticket.getApprover().getEmail());
            to = new String[2];
            to[0] = emails.get(0);
            to[1] = emails.get(1);
            mailWorker.sendMail(to, subj, message);
        } else if (user.getRole() == Role.MANAGER) {
            message += " by the Manager";
            to = new String[1];
            to[0] = emails.get(0);
            mailWorker.sendMail(to, subj, message);
        } else {
            message += " by you";
            to = new String[1];
            to[0] = emails.get(0);
            mailWorker.sendMail(to, subj, message);
        }
    }

    @Override
    public void sendTicketWasDone(Ticket ticket) throws MessagingException {
        String[] to = {ticket.getOwner().getEmail()};
        String subj = "Ticket was done";
        String message =
            "Dear " + ticket.getOwner().getFirstName() + " " + ticket.getOwner().getLastName()
                + "<br><br>\n" +
                "\n" +
                "Ticket <a href='http://localhost:8081/overview/" + ticket.getId() + "'>"
                + ticket
                .getName() + "</a> was done by the Engineer.<br>\n" +
                "Please provide your feedback clicking on the ticket ID.\n";
        mailWorker.sendMail(to, subj, message);
    }

    @Override
    public void sendFeedbackWasProvide(Feedback feedback) throws MessagingException {
        Ticket ticket = feedback.getTicket();
        String[] to = {ticket.getAssignee().getEmail()};
        String subj = "Feedback was provided";
        String message =
            "Dear " + ticket.getAssignee().getFirstName() + " " + ticket.getAssignee().getLastName()
                + "<br><br>\n" +
                "\n" +
                "The feedback was provided on ticket <a href='http://localhost:8081/overview/"
                + ticket.getId() + "'>" + ticket.getName() + "</a>";
        mailWorker.sendMail(to, subj, message);
    }
}
