package com.diruch.services.impl;

import com.diruch.exceptions.InvalidFileException;
import com.diruch.models.Attachment;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.repository.AttachmentRepository;
import com.diruch.services.AttachmentService;
import com.diruch.services.HistoryService;
import com.diruch.services.TicketService;
import com.diruch.services.UserService;
import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class AttachmentServiceImpl implements AttachmentService {

    @Autowired
    private AttachmentRepository attachmentRepository;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private UserService userService;

    @Autowired
    private HistoryService historyService;

    @Override
    public void save(Attachment attachment, User user) {
        attachmentRepository.save(attachment);
    }

    @Override
    public List<Attachment> getAttachmentsByTicketId(int ticketId) {
        return attachmentRepository.getAttachmetsByTicketId(ticketId);
    }

    @Override
    public Attachment getAttachmentById(int attachId) {
        return attachmentRepository.getAttachmentById(attachId);
    }

    @Override
    public void saveAttachments(MultipartFile[] files, int ticketId, String username)
        throws IOException, InvalidFileException {
        Ticket ticket = ticketService.getTicketById(ticketId);
        User user = userService.loadUserByUsername(username);
        for (MultipartFile file : files) {
            String[] name = file.getOriginalFilename().split("\\.");
            String ext = name[name.length - 1];
            System.out.println(ext);
            String extens = "pdf,jpeg,jpg,doc,docx,png";
            if (file.getSize() > 5 * 1024 * 1024 || !extens.contains(ext)) {
                throw new InvalidFileException("Wrong size or extension!");
            }
            Attachment attachment = new Attachment();
            attachment.setBlob(file.getBytes());
            attachment.setContentType(file.getContentType());
            attachment.setName(file.getOriginalFilename());
            attachment.setTicket(ticket);
            historyService.historyAttachFile(ticket, user, file.getOriginalFilename());
            save(attachment, user);
        }
    }

    @Override
    public void delete(int attachId, String username) {
        Attachment attachment = getAttachmentById(attachId);
        attachmentRepository.delete(getAttachmentById(attachId));
        Ticket ticket = attachment.getTicket();
        User user = userService.loadUserByUsername(username);
        historyService.historyRemoveFile(ticket, user, attachment.getName());
    }
}
