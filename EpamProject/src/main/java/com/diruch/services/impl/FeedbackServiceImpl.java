package com.diruch.services.impl;

import com.diruch.models.Feedback;
import com.diruch.repository.FeedbackRepository;
import com.diruch.services.FeedbackService;
import com.diruch.services.MailService;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    private FeedbackRepository feedbackRepository;

    @Autowired
    private MailService mailService;


    @Override
    public void save(Feedback feedback) throws MessagingException {
        mailService.sendFeedbackWasProvide(feedback);
        feedbackRepository.save(feedback);
    }

    @Override
    public Feedback getFeedbackByTicketId(int ticketId) {
        return feedbackRepository.getFeedbackByTicketId(ticketId);
    }
}
