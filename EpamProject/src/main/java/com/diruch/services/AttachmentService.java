package com.diruch.services;


import com.diruch.exceptions.InvalidFileException;
import com.diruch.models.Attachment;
import com.diruch.models.User;
import java.io.IOException;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public interface AttachmentService {

    void save(Attachment attachment, User user);

    List<Attachment> getAttachmentsByTicketId(int ticketId);

    Attachment getAttachmentById(int attachId);

    void saveAttachments(MultipartFile[] files, int ticketId, String username)
        throws IOException, InvalidFileException;

    void delete(int attachId, String username);

}
