package com.diruch.services;

import com.diruch.dto.HistoryDto;
import com.diruch.enums.State;
import com.diruch.models.History;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import java.util.List;

public interface HistoryService {

    void save(History history);

    List<HistoryDto> getHistoryByTicketId(int ticketId);

    void historyCreateTicket(Ticket ticket, User user);

    void historyEditTicket(Ticket ticket, User user);

    void historyChangeStatus(Ticket ticket, User user, State to);

    void historyAttachFile(Ticket ticket, User user, String fileName);

    void historyRemoveFile(Ticket ticket, User user, String fileName);
}
