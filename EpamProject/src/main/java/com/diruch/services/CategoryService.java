package com.diruch.services;

import com.diruch.models.Category;
import java.util.List;

public interface CategoryService {

    Category getCategoryById(int id);

    List<Category> getAllCategories();
}
