package com.diruch.services;

import com.diruch.models.Feedback;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import javax.mail.MessagingException;

public interface MailService {

    void sendTicketsForApprove(Ticket ticket) throws MessagingException;

    void sendTicketWasApprove(Ticket ticket) throws MessagingException;

    void sendTicketWasDeclined(Ticket ticket) throws MessagingException;

    void sendTicketWasCancelled(Ticket ticket, User user) throws MessagingException;

    void sendTicketWasDone(Ticket ticket) throws MessagingException;

    void sendFeedbackWasProvide(Feedback feedback) throws MessagingException;

}
