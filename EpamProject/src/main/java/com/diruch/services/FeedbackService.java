package com.diruch.services;

import com.diruch.models.Feedback;
import javax.mail.MessagingException;

public interface FeedbackService {

    void save(Feedback feedback) throws MessagingException;

    Feedback getFeedbackByTicketId(int ticketId);
}
