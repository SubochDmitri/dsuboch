package com.diruch.services;

import com.diruch.dto.TicketDto;
import com.diruch.dto.UserDto;
import com.diruch.models.User;
import java.util.List;

public interface UserService {

    User loadUserByUsername(String username);

    List<TicketDto> getManagerTicketsDto(User user);

    List<TicketDto> getOwnerTicketsDto(User user);

    List<TicketDto> getEngineerTicketsDto(User user);

    List<TicketDto> getAcceptedTickets(User user);

    UserDto getCurrentUser(String username);

    List<User> getAllManagers();

    List<User> getAllEngineers();
}
