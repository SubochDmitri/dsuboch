package com.diruch.services;

import com.diruch.dto.CommentDto;
import com.diruch.exceptions.InvalidDataException;
import com.diruch.models.Comment;
import java.util.List;

public interface CommentService {

    void save(Comment comment);

    List<CommentDto> getCommentsByTicketId(int ticketId);

    void saveCommentByUser(CommentDto commentDto, String username) throws InvalidDataException;
}
