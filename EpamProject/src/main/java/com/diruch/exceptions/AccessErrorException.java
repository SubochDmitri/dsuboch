package com.diruch.exceptions;

public class AccessErrorException extends  Exception {
    public AccessErrorException (String message) {super(message);}
}
