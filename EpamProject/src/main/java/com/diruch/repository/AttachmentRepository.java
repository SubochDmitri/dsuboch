package com.diruch.repository;

import com.diruch.models.Attachment;
import java.util.List;

public interface AttachmentRepository {

    List<Attachment> getAttachmetsByTicketId(int ticketId);

    Attachment getAttachmentById(int attachId);

    void save(Attachment attachment);

    void delete(Attachment attachment);
}
