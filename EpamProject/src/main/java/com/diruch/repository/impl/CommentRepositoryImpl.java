package com.diruch.repository.impl;

import com.diruch.models.Comment;
import com.diruch.repository.CommentRepository;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CommentRepositoryImpl implements CommentRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Comment> getCommentsByTicketId(int ticketId) {
        String hql = "from Comment where ticket.id=:ticketid";
        List<Comment> ticketComments = sessionFactory.getCurrentSession().createQuery(hql)
            .setParameter("ticketid", ticketId)
            .list();
        return ticketComments;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(Comment comment) {
        sessionFactory.getCurrentSession().save(comment);
    }
}
