package com.diruch.repository.impl;

import com.diruch.enums.Role;
import com.diruch.enums.State;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.repository.UserRepository;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public User loadUserByUsername(String username) {
        String hql = "from User where email=:username";
        User user = (User) sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setString("username", username)
            .uniqueResult();
        return user;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Ticket> getManagerTickets(User user) {
        String hql = "from Ticket where owner.id=:ownerId  or state=:newState or (approver.id=:approverId and state not in (:stateNew,:draft))";
        List<Ticket> tickets = sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("ownerId", user.getId())
            .setParameter("newState", State.NEW)
            .setParameter("approverId", user.getId())
            .setParameter("stateNew", State.NEW)
            .setParameter("draft", State.DRAFT)
            .list();
        return tickets;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Ticket> getOwnerTickets(User user) {
        String hql = "from Ticket where owner.id = :ownerId";
        List<Ticket> tickets = sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("ownerId", user.getId())
            .list();
        return tickets;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Ticket> getEngineerTickets(User user) {
        String hql = "from Ticket where state=:approved or (assignee.id=:aid and state=:inprogr)";
        List<Ticket> tickets = sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("approved", State.APPROVED)
            .setParameter("aid", user.getId())
            .setParameter("inprogr", State.IN_PROGRESS)
            .list();
        return tickets;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Ticket> getAcceptedTickets(User user) {
        String hql = "from Ticket where assignee.id = :assigneeId";
        List<Ticket> tickets = sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("assigneeId", user.getId())
            .list();
        return tickets;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<User> getAllManagers() {
        String hql = "from User where role = :role";
        List<User> users = sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("role", Role.MANAGER)
            .list();
        return users;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<User> getAllEngineers() {
        String hql = "from User where role = :role";
        List<User> users = sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("role", Role.ENGINEER)
            .list();
        return users;
    }
}
