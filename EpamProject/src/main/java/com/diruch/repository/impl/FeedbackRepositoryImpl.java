package com.diruch.repository.impl;

import com.diruch.models.Feedback;
import com.diruch.repository.FeedbackRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class FeedbackRepositoryImpl implements FeedbackRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(Feedback feedback) {
        sessionFactory.getCurrentSession().save(feedback);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Feedback getFeedbackByTicketId(int ticketId) {
        String hql = "from Feedback where ticket.id=:ticketid";
        Feedback feedback = (Feedback) sessionFactory.getCurrentSession().createQuery(hql)
            .setParameter("ticketid", ticketId)
            .uniqueResult();
        return feedback;
    }
}
