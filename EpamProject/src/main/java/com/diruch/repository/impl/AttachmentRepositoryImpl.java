package com.diruch.repository.impl;

import com.diruch.models.Attachment;
import com.diruch.repository.AttachmentRepository;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class AttachmentRepositoryImpl implements AttachmentRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Attachment> getAttachmetsByTicketId(int ticketId) {
        String hql = "From Attachment where ticket.id=:ticketId";
        List<Attachment> attachments = (List<Attachment>) sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("ticketId", ticketId)
            .list();
        return attachments;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Attachment getAttachmentById(int attachId) {
        String hql = "From Attachment where id=:attachId";
        Attachment attachment = (Attachment) sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("attachId", attachId)
            .uniqueResult();
        return attachment;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(Attachment attachment) {
        sessionFactory.getCurrentSession().save(attachment);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void delete(Attachment attachment) {
        sessionFactory.getCurrentSession().delete(attachment);
    }
}
