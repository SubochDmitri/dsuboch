package com.diruch.repository.impl;

import com.diruch.enums.State;
import com.diruch.models.Ticket;
import com.diruch.models.User;
import com.diruch.repository.TicketRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class TicketRepositoryImpl implements TicketRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(Ticket ticket) {
        sessionFactory.getCurrentSession().save(ticket);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Ticket getTicketById(int ticketId) {
        String hql = "from Ticket where id=:ticketId";
        Ticket ticket = (Ticket) sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("ticketId", ticketId)
            .uniqueResult();
        return ticket;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void delete(Ticket ticket) {
        sessionFactory.getCurrentSession().delete(ticket);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void update(Ticket ticket) {
        sessionFactory.getCurrentSession().merge(ticket);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void submit(Ticket ticket, User user) {
        ticket.setOwner(user);
        ticket.setState(State.NEW);
        sessionFactory.getCurrentSession().merge(ticket);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void cancel(Ticket ticket, User user) {
        switch (user.getRole()) {
            case MANAGER:
                ticket.setApprover(user);
                break;
            case ENGINEER:
                ticket.setAssignee(user);
                break;
        }
        ticket.setState(State.CANCELED);
        sessionFactory.getCurrentSession().merge(ticket);
    }


    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void approve(Ticket ticket, User user) {
        ticket.setApprover(user);
        ticket.setState(State.APPROVED);
        sessionFactory.getCurrentSession().merge(ticket);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void decline(Ticket ticket, User user) {
        ticket.setApprover(user);
        ticket.setState(State.DECLINED);
        sessionFactory.getCurrentSession().merge(ticket);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void assign(Ticket ticket, User user) {
        ticket.setAssignee(user);
        ticket.setState(State.IN_PROGRESS);
        sessionFactory.getCurrentSession().merge(ticket);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void done(Ticket ticket) {
        ticket.setState(State.DONE);
        sessionFactory.getCurrentSession().merge(ticket);
    }


}
