package com.diruch.repository.impl;

import com.diruch.models.Category;
import com.diruch.repository.CategoryRepository;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Category getCategoryById(int id) {
        String hql = "from Category where id=:catId";
        Category category = (Category) sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("catId", id)
            .uniqueResult();
        return category;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<Category> getAllCategories() {
        String hql = "from Category";
        List<Category> categories = sessionFactory.getCurrentSession()
            .createQuery(hql)
            .list();
        return categories;
    }
}
