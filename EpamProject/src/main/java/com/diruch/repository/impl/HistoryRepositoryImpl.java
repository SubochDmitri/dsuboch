package com.diruch.repository.impl;

import com.diruch.models.History;
import com.diruch.repository.HistoryRepository;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class HistoryRepositoryImpl implements HistoryRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public List<History> getHistoryByTicketId(int ticketId) {
        String hql = "from History item where item.ticket.id=:ticketid";
        List<History> history = sessionFactory.getCurrentSession()
            .createQuery(hql)
            .setParameter("ticketid", ticketId)
            .list();
        return history;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void save(History history) {
        sessionFactory.getCurrentSession().save(history);
    }
}
