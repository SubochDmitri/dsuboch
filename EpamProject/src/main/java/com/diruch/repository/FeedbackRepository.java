package com.diruch.repository;

import com.diruch.models.Feedback;

public interface FeedbackRepository {

    void save(Feedback feedback);

    Feedback getFeedbackByTicketId(int ticketId);
}
