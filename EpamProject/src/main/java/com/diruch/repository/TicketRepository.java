package com.diruch.repository;

import com.diruch.models.Ticket;
import com.diruch.models.User;

public interface TicketRepository {

    void save(Ticket ticket);

    Ticket getTicketById(int ticketId);

    void delete(Ticket ticket);

    void update(Ticket ticket);

    void submit(Ticket ticket, User user);

    void cancel(Ticket ticket, User user);

    void approve(Ticket ticket, User user);

    void decline(Ticket ticket, User user);

    void assign(Ticket ticket, User user);

    void done(Ticket ticket);
}
