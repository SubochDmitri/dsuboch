package com.diruch.repository;

import com.diruch.models.Category;
import java.util.List;

public interface CategoryRepository {

    Category getCategoryById(int id);

    List<Category> getAllCategories();
}
