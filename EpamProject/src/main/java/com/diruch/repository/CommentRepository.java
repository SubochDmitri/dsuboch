package com.diruch.repository;

import com.diruch.models.Comment;
import java.util.List;

public interface CommentRepository {

    List<Comment> getCommentsByTicketId(int ticketId);

    void save(Comment comment);

}
