package com.diruch.repository;

import com.diruch.models.History;
import java.util.List;

public interface HistoryRepository {

    List<History> getHistoryByTicketId(int ticketId);

    void save(History history);
}
