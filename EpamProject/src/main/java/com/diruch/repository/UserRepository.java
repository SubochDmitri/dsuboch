package com.diruch.repository;

import com.diruch.models.Ticket;
import com.diruch.models.User;
import java.util.List;

public interface UserRepository {

    User loadUserByUsername(String username);

    List<Ticket> getManagerTickets(User user);

    List<Ticket> getOwnerTickets(User user);

    List<Ticket> getEngineerTickets(User user);

    List<Ticket> getAcceptedTickets(User user);

    List<User> getAllManagers();

    List<User> getAllEngineers();
}
