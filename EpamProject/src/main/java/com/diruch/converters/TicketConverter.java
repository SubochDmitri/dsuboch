package com.diruch.converters;

import com.diruch.dto.TicketDto;
import com.diruch.exceptions.InvalidDataException;
import com.diruch.models.Ticket;
import com.diruch.validator.TextValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TicketConverter {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TextValidator validator;

    public List<TicketDto> convertListInDto(List<Ticket> tickets) {
        List<TicketDto> ticketDtos = new ArrayList<>();
        for (Ticket ticket : tickets) {
            ticketDtos.add(convertToDto(ticket));
        }
        return ticketDtos;
    }

    public TicketDto convertToDto(Ticket ticket) {
        TicketDto ticketDto;
        if (ticket != null) {
            ticketDto = TicketDto.builder()
                .id(ticket.getId())
                .name(ticket.getName())
                .description(ticket.getDescription())
                .createdOn(ticket.getCreatedOn())
                .desiredResolutionDate(ticket.getDesiredResolutionDate())
                .assignee(ticket.getAssignee())
                .owner(ticket.getOwner())
                .state(ticket.getState())
                .category(ticket.getCategory())
                .urgency(ticket.getUrgency())
                .approver(ticket.getApprover())
                .build();
            return ticketDto;
        } else {
            return null;
        }
    }

    public Ticket convertFromDto(TicketDto ticketDto) throws InvalidDataException {
        Ticket ticket = new Ticket();
        if (ticketDto != null && validator.isValidName(ticketDto.getName()) && validator
            .isValid(ticketDto.getDescription())) {
            if (ticketDto.getId() == null) {
                ticket.setId(0);
            } else {
                ticket.setId(ticketDto.getId());
            }
            ticket.setName(ticketDto.getName());
            ticket.setCategory(ticketDto.getCategory());
            ticket.setDescription(ticketDto.getDescription());
            ticket.setDesiredResolutionDate(ticketDto.getDesiredResolutionDate());
            if (ticket.getDesiredResolutionDate() == null) {
                ticket.setDesiredResolutionDate(new Date(new java.util.Date().getTime()));
            }
            ticket.setOwner(ticketDto.getOwner());
            ticket.setUrgency(ticketDto.getUrgency());
            ticket.setState(ticketDto.getState());
        } else {
            throw new InvalidDataException("Wrond name or description");
        }
        return ticket;
    }

    public TicketDto convertFromJSON(String json) throws IOException {

        if (!json.isEmpty()) {
            TicketDto ticketDto = objectMapper.readValue(json, TicketDto.class);
            return ticketDto;
        } else {
            return null;
        }
    }


}
