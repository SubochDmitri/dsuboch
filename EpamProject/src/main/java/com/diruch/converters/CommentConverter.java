package com.diruch.converters;

import com.diruch.dto.CommentDto;
import com.diruch.exceptions.InvalidDataException;
import com.diruch.models.Comment;
import com.diruch.validator.TextValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentConverter {

    @Autowired
    private TextValidator validator;

    public CommentDto convertToDto(Comment comment) {
        CommentDto commentDto = null;
        if (comment != null) {
            commentDto = CommentDto.builder()
                .date(comment.getDate())
                .text(comment.getText())
                .ticket(comment.getTicket())
                .user(comment.getUser())
                .id(comment.getId())
                .build();
        }
        return commentDto;
    }

    public Comment convertFromDto(CommentDto commentDto) throws InvalidDataException {
        Comment comment = new Comment();

        if (commentDto != null && validator.isValid(commentDto.getText())) {
            comment.setId(commentDto.getId());
            comment.setText(commentDto.getText());
            comment.setTicket(commentDto.getTicket());
            comment.setUser(commentDto.getUser());
        } else {
            throw new InvalidDataException("Wrong comment");
        }
        return comment;
    }

}
