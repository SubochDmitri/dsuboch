package com.diruch.converters;

import com.diruch.dto.TicketDto;
import com.diruch.dto.UserDto;
import com.diruch.models.User;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class UserConverter {

    public UserDto convertToDto(User user, List<TicketDto> allTickets, List<TicketDto> myTickets) {
        UserDto userDto;
        if (user != null) {
            userDto = UserDto.builder()
                .email(user.getEmail())
                .id(user.getId())
                .role(user.getRole())
                .allTickets(allTickets)
                .myTickets(myTickets)
                .build();
            return userDto;
        }
        return null;
    }
}
