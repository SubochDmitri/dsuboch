package com.diruch.converters;

import com.diruch.dto.HistoryDto;
import com.diruch.models.History;
import org.springframework.stereotype.Component;

@Component
public class HistoryConverter {

    public HistoryDto convertToDto(History history) {
        HistoryDto historyDto = null;
        if (history != null) {
            historyDto = HistoryDto.builder()
                .action(history.getAction())
                .date(history.getDate())
                .description(history.getDescription())
                .userFirstName(history.getUser().getFirstName())
                .userLastName(history.getUser().getLastName())
                .build();
        }
        return historyDto;
    }

}
