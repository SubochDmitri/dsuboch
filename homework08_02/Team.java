import java.util.ArrayList;

public class Team {
    private ArrayList<Worker> workers = new ArrayList<>();
    private double service_cost;

    void addWorker(Worker worker){
        workers.add(worker);
    }

    void setService_cost(double service_cost) {
        this.service_cost = service_cost;
    }

    ArrayList<Worker> getWorkers() {
        return workers;
    }

    double getService_cost() {
        return service_cost;
    }
}
