import java.util.ArrayList;

public class Demands implements Cloneable{
    private ArrayList<Skills> demands  = new ArrayList<>();

    ArrayList<Skills> getDemands() {
        return demands;
    }

    void addDemand(Skills skill){
        demands.add(skill);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
