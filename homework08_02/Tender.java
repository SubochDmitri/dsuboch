import java.util.ArrayList;

public class Tender {
    private ArrayList<Team> teams = new ArrayList<>();
    private Demands demands;

    Tender(Demands demands){
        this.demands = demands;
    }

    void addTeam(Team team){
        teams.add(team);
    }

    Team searchProfitableTeam(){
        Team profitTeam = null;
        for (int i=0;i<teams.size();i++){
            Demands temp_demands = null;
            try {
                temp_demands = (Demands)demands.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            for (int j=0;j<teams.get(i).getWorkers().size();j++){
                for(int k=0;k<teams.get(i).getWorkers().get(j).getSkills().size();k++){
                    temp_demands.getDemands().remove(teams.get(i).getWorkers().get(j).getSkills().get(k));
                }
            }
            if (profitTeam!=null&&(profitTeam.getService_cost()>teams.get(i).getService_cost())&&(temp_demands.getDemands().size()==0)){
                profitTeam = teams.get(i);
            }else if ((temp_demands.getDemands().size()==0)&&(profitTeam==null)){
                profitTeam = teams.get(i);
            }
        }
        return profitTeam;
    }


}
