public class Main {
    public static void main(String[] args) {
        Worker w1 = new Worker();
        w1.addSkill(Skills.BUILDER);
        w1.addSkill(Skills.DRIVER);
        Worker w2 = new Worker();
        w2.addSkill(Skills.BUILDER);
        w2.addSkill(Skills.DRIVER);
        Team t1 = new Team();
        t1.addWorker(w1);
        t1.addWorker(w2);
        t1.setService_cost(1);
        Worker w3 = new Worker();
        w3.addSkill(Skills.BUILDER);
        w3.addSkill(Skills.DRIVER);
        Worker w4 = new Worker();
        w4.addSkill(Skills.BUILDER);
        w4.addSkill(Skills.DRIVER);
        Team t2 = new Team();
        t2.addWorker(w3);
        t2.addWorker(w4);
        t2.setService_cost(10);

        Demands demands = new Demands();
        demands.addDemand(Skills.BUILDER);
        demands.addDemand(Skills.DRIVER);

        Tender tender = new Tender(demands);
        tender.addTeam(t1);
        tender.addTeam(t2);
        Team a = tender.searchProfitableTeam();

    }
}
