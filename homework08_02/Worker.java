import java.util.ArrayList;

public class Worker {

    private ArrayList<Skills> skills = new ArrayList<>();

    void addSkill(Skills skill){
        skills.add(skill);
    }

    ArrayList<Skills> getSkills() {
        return skills;
    }
}
