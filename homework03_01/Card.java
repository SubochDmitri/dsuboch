public class Card {
    private double balance;
    private String name;

    Card (double balance, String name)
    {
        this.balance = balance;
        this.name = name;
    }

    public Card (String name)
    {
        this.name = name;
        balance = 0;
    }

    double getBalance()
    {
        return balance;
    }

    public void increaseBalance(double size)
    {
        balance += size;
    }

    public void decreaseBalance(double size)
    {
        balance -= size;
    }

    public void showBalanceInCurrency(double coof)
    {
        System.out.println(coof*balance);
    }
}
