public class Main {
    public static void main(String[] args) {
        Card card = new Card(100,"Kot");
        card.increaseBalance(10);
        card.decreaseBalance(20);
        card.showBalanceInCurrency(1.5);
    }
}
