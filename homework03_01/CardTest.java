import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class CardTest {
    @Test
    void getBalance() {
        assertEquals(10, new Card(10,"vasia").getBalance());
    }

}