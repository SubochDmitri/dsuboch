import java.util.Arrays;

public class Median {
    static double median(double[] mass)
    {
        double mediana;
        Arrays.sort(mass);
        int len = mass.length;
        if (len % 2 == 0) mediana = (mass[len/2] + mass[len/2-1]) / 2;
        else mediana = mass[len/2];
        return mediana;
    }

    static float median(int[] mass)
    {
        int i;
        i=0;
        float mediana;
        Arrays.sort(mass);
        int len = mass.length;
        if (len % 2 == 0) {
            mediana = (mass[len / 2] + mass[len / 2 - 1]);
            return mediana/2;
        }
        else mediana = mass[len/2];
        return mediana;
    }


}
