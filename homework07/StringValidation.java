import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidation implements Validator<String> {
    @Override
    public boolean validate(String text) throws ValidationFailedException {
        Pattern pattern = Pattern.compile("^[A-Z].*$");
        Matcher matcher = pattern.matcher(text);
        if (matcher.matches()) {
            return true;
        } else {
            throw new ValidationFailedException();
        }
    }
}
