import java.util.Set;

public interface Validator<T> {
    public boolean validate(T element) throws ValidationFailedException;

}
