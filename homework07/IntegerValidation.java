public class IntegerValidation implements Validator<Integer> {
    @Override
    public boolean validate(Integer element) throws ValidationFailedException {
        if ((element >= 1) && (element <= 10)) {
            return true;
        } else {
            throw new ValidationFailedException();
        }
    }
}
