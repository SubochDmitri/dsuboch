public final class ValidationSystem<T> {
    private static IntegerValidation integerValidation = new IntegerValidation();
    private static StringValidation stringValidation = new StringValidation();
    private ValidationSystem() {
        throw new AssertionError("Not for initialization");
    }

    public static <T> void validate(T data) throws ValidationFailedException {
        if(data==null){
            throw new ValidationFailedException();
        }
        if (data instanceof String) {
            stringValidation.validate((String) data);
        }
        if (data instanceof Integer) {
            integerValidation.validate((Integer) data);
        }
    }
}
