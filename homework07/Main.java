public class Main {
    public static void main(String[] args) throws ValidationFailedException {
        ValidationSystem.validate("text");
        ValidationSystem.validate("Text");
        ValidationSystem.validate(1);
        ValidationSystem.validate(44);
    }
}
