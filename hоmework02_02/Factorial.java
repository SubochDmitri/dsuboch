public class Factorial {
    Factorial(int id_cycle, int n){
        switch (id_cycle){
            case 1:
                whileRes(n);
                break;
            case 2:
                doWhileRes(n);
                break;
            case 3:
                forRes(n);
                break;
            default:
                System.out.println("Вы ввели неверный параметр!");

        }
    }

    private void whileRes(int n){
        long result = 1;

        if (n == 0 || n == 1)
            System.out.println(n+"!  = "+result);
        int i=2;
        while(i<=n)
        {
            result = result * i;
            if (i >= n)
            {
                System.out.println(n+"!  = "+result);
            }
            i++;
        }
    }

    private void doWhileRes(int n){
        long result = 1;

        if (n == 0 || n == 1)
            System.out.println(n+"!  = "+result);
        int i=2;
        do
        {
            result = result * i;
            if (i >= n)
            {
                System.out.println(n+"!  = "+result);
            }
            i++;
        } while(i<=n);

    }

    private void forRes(int n){

        long result = 1;

        if (n == 0 || n == 1)
            System.out.println(n+"!  = "+result);

        for (int i = 2; i <= n; i++)
        {
            result = result * i;
            if (i >= n)
            {
                System.out.println(n+"!  = "+result);
            }
        }

    }
}
