
public class Main {
    public static void main(String[] args) {
        int id_alg = Integer.parseInt(args[0]);
        int id_cycle = Integer.parseInt(args[1]);
        int n = Integer.parseInt(args[2]);
        switch (id_alg){
            case 1:
                Fibonacci fib = new Fibonacci(id_cycle, n);
                break;
            case 2:
                Factorial fac = new Factorial(id_cycle,n);
                break;
            default:
                System.out.println("Вы ввели неверные параметры");
        }



    }
}
