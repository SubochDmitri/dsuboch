public class Fibonacci {
    private static int a1 = 1;
    private static int a2 = 1;


    Fibonacci(int id_cycle, int n){
        switch (id_cycle){
            case 1:
                whileRes(n);
                break;
            case 2:
                doWhileRes(n);
                break;
            case 3:
                forRes(n);
                break;
            default:
                System.out.println("Вы ввели неверный параметр!");

        }
    }

    private void whileRes(int n){
        System.out.print(a1+" "+a2+" ");
        int sum=0;
        int i=0;
        while(i<n-2){
            sum = a1+a2;
            System.out.print(sum+" ");
            a1=a2;
            a2=sum;
            i++;
        }
    }

    private void doWhileRes(int n){
        System.out.print(a1+" "+a2+" ");
        int sum=0;
        int i=0;
        do{
            sum = a1+a2;
            System.out.print(sum+" ");
            a1=a2;
            a2=sum;
            i++;
        } while(i<n-2);

    }

    private void forRes(int n){
        System.out.print(a1+" "+a2+" ");
        int sum=0;
        for(int i = 0; i < n-2; i++){
            sum = a1+a2;
            System.out.print(sum+" ");
            a1=a2;
            a2=sum;
        }

    }
}
