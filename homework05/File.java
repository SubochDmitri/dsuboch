public class File {
    private String name;
    private String extension;

    File(String name, String extension){
        this.name = name;
        this.extension = extension;
    }

    @Override
    public String toString() {
        return name+"."+extension;
    }
}