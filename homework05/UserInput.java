import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class UserInput {

        static String[] readLine(){
        InputStreamReader cin = new InputStreamReader(System.in);
        BufferedReader bf = new BufferedReader(cin);
        String path="";
        System.out.print("$>");
        try {
            path = bf.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] dirs = path.split("/");

        return dirs;
    }
}
