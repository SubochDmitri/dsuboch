import java.util.ArrayList;

public class Core {
    private final static Folder folder = new Folder();

    void input(){
        String[] dirs = UserInput.readLine();
        folder.setName(dirs[0]);
        addPathes(dirs,1,dirs.length,folder);
        while (true){
            String[] ndirs = UserInput.readLine();
            if (ndirs[0].equals("print")) {
                folder.setCount(folder.getList_folders().size());
                Output.print(folder);
            }
            else if (ndirs[0].equals("exit")) System.exit(0);
            searchFolder(ndirs);
        }
    }

    private static void searchFolder(String[] folders){
        ArrayList<Folder> list = folder.getList_folders();
        Folder parentFolder = folder;
        for (int i=1;i<folders.length;i++){
            for (int j=0;j<list.size();j++){
                if (list.get(j).getName().equals(folders[i])){
                    parentFolder = list.get(j);
                    list = parentFolder.getList_folders();
                    break;
                }
                if (j==list.size()-1){
                    addPathes(folders,i,folders.length,parentFolder);

                    i=folders.length-1;
                    break;
                }
            }
            if(parentFolder.getList_folders().size()==0){
                addPathes(folders,i+1,folders.length,parentFolder);

                break;
            }
        }
    }

    private static void addPathes(String[] dirs, int start, int lenght, Folder parentFolder) {
        File file = null;
        if (dirs[lenght - 1].contains(".")) {
            String[] files = {"", ""};
            lenght--;
            String tmp = dirs[lenght];
            int j = 0;
            for (int i = 0; i < tmp.length(); i++) {
                if (tmp.charAt(i) == '.') {
                    i++;
                    j++;
                }
                files[j] += tmp.charAt(i);

            }
            file = new File(files[0], files[1]);
        }
        if ((lenght - start == 0) && (file != null)) {
            parentFolder.addFile(file);
        }
        for (int i = start; i < lenght; i++) {

            Folder newFolder = new Folder();
            newFolder.setName(dirs[i]);
            parentFolder.setCountTab(i);
            parentFolder.setCheck(false);
            parentFolder.setCount(parentFolder.getList_folders().size());
            parentFolder.addCatalog(newFolder);
            parentFolder = newFolder;
            if ((file != null) && (i == lenght - 1)) {
                newFolder.addFile(file);            }
            if (i == lenght - 1) {
                parentFolder.setLast(true);
                parentFolder.setCountTab(i + 1);
                parentFolder.setCheck(false);
                parentFolder.setCount(parentFolder.getList_folders().size());
            }
        }
    }
}
