import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public final class FileWorker {
    private FileWorker(){

    }
    public static boolean save(Folder folder){
        try {
            FileOutputStream outputStream = new FileOutputStream("temp.out",false);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(folder);
            objectOutputStream.flush();
            objectOutputStream.close();
            return true;
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    public static Folder load(){
        Folder folder=null;
        try {
            FileInputStream fileInputStream = new FileInputStream("temp.out");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            folder= (Folder) objectInputStream.readObject();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return folder;
    }
}
