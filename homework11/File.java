import java.io.Serializable;

public class File implements Serializable {
    private String name;
    private String extension;

    File(String name, String extension){
        this.name = name;
        this.extension = extension;
    }

    @Override
    public String toString() {
        return name+"."+extension;
    }
}