import java.util.ArrayList;

class Output {
    private static ArrayList<String> listname = new ArrayList<>();
    static void print(Folder folder1){
        Folder folder=folder1.clone();
        int i=-1;
        while ((i<folder.getList_folders().size())&&(folder.getName()!=null)){
            i++;
            if ((folder.getCount()>=0) || (folder.isLast())){
                if (!folder.isCheck()){
                    System.out.println(tab(folder.getCountTab()-1)+folder+"/");
                    for (int il=0;il<folder.getList_files().size();il++){
                        System.out.println(tab(folder.getCountTab())+folder.getList_files().get(il));
                    }
                    folder.setCheck(true);
                    listname.add(folder.getName());
                }
                folder.decCount();
                try{
                    print(folder.getList_folders().get(i).clone());
                } catch (Exception e){

                }
            }
        }
    }
    private static String tab(int k){
        String s="";
        for (int i = 0; i < k; i++){
            s+="\t";
        }
        return s;
    }
}
