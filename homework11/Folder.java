import java.io.Serializable;
import java.util.ArrayList;

public class Folder implements Cloneable, Serializable {
    private String name;
    private ArrayList<Folder> list_folders = new ArrayList<>();
    private ArrayList<File> list_files = new ArrayList<>();
    private int count;
    private boolean last;
    private boolean check;
    private int countTab;

    void decCount() {
        this.count--;
    }

    int getCountTab() {
        return countTab;
    }

    void setCountTab(int count) {
        this.countTab = count;
    }

    boolean isCheck(){
        return check;
    }

    void setCheck(boolean check){
        this.check = check;
    }

    int getCount(){
        return count;
    }

    void setCount(int count){
        this.count=count;
    }

    void setLast(boolean bool){
        this.last=bool;
    }

    boolean isLast(){
        return last;
    }

    @Override
    public Folder clone() {
        try {
            return (Folder) super.clone();
        }
        catch( CloneNotSupportedException ex ) {
            throw new InternalError();
        }
    }

    void setName(String name){
        this.name = name;
    }

    void addCatalog(Folder folder){
        list_folders.add(folder);
    }


    void addFile(File file) {
        list_files.add(file);
    }

    ArrayList<File> getList_files() {
        return list_files;
    }

    ArrayList<Folder> getList_folders() {
        return list_folders;
    }

    String getName(){
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
