public class ATMmoneyConsumer {



    void startDecrease() {
    Thread consumer = new Thread(new Runnable() {
        @Override
        public void run() {

            while (ATMstart.flag) {
                double value = Rand.rnd(5, 10);
                Main.getCard().decreaseBalance(value);
                if (Main.getCard().getBalance()<=0){
                    ATMstart.flag = false;
                    Print.print("Money is over");
                }
                try {
                    Thread.sleep(Rand.rnd(2000,5000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    });
    consumer.start();
    }
}
