import java.util.ArrayList;

public final class ATMstart {
    private ATMstart(){
        throw new AssertionError("Not for instantiation!");
    }
    static boolean flag=true;

    static void runATMs(){
        ArrayList<ATMmoneyProducer> atms_prod = new ArrayList<>();
        ArrayList<ATMmoneyConsumer> atms_cons = new ArrayList<>();
        int n = Rand.rnd(3,5);
        System.out.println("Producers created "+n);
        int m = Rand.rnd(3,5);
        System.out.println("Consumers created "+m);
        for (int i=0;i<n;i++){
            atms_prod.add(new ATMmoneyProducer());
            atms_prod.get(i).startIncrease();
        }
        for (int i=0;i<m;i++){
            atms_cons.add(new ATMmoneyConsumer());
            atms_cons.get(i).startDecrease();
        }
    }


}
