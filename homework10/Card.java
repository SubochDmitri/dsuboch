public class Card {

    private double balance=500;


    double getBalance() {
        return balance;
    }

    synchronized void decreaseBalance(double value){
        balance -= value;
        Print.printDecrease(value);
    }

    synchronized void increaseBalance(double value){
        balance += value;
        Print.printIncrease(value);
    }
}
