import java.util.Random;

public  class ATMmoneyProducer {

    void startIncrease() {
        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {

                while (ATMstart.flag) {
                    double value = Rand.rnd(5, 10);
                    Main.getCard().increaseBalance(value);
                    if ((Main.getCard().getBalance() > 1000)) {
                        ATMstart.flag = false;
                        Print.print("Current balance more than 1000");
                    }
                    try {
                        Thread.sleep(Rand.rnd(2000, 5000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        producer.start();
    }

}
