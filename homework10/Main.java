import java.util.ArrayList;

public class Main {
    private static Card card;

    static Card getCard() {
        return card;
    }

    public static void main(String[] args) {
        card = new Card();
       ATMstart.runATMs();
    }

}
