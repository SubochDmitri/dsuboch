public final class Rand {
    private Rand(){
        throw new AssertionError("Not for instantiation!");
    }

    static int rnd(int min, int max)
    {
        max -= min;
        return (int) (Math.random() * ++max) + min;
    }
}
