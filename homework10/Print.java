public final class Print {
    private Print(){
        throw new AssertionError("Not for instantiation!");
    }

    static void printIncrease(double value){
        System.out.println("Balance increased on "+value+" ------- Current balance: "+Main.getCard().getBalance());
    }

    static void printDecrease(double value){
        System.out.println("Balance decreased on "+value+" ------- Current balance: "+Main.getCard().getBalance());
    }

    static void print(String value){
        System.out.println(value);
    }
}
