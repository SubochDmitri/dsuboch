import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomList<E> extends ArrayList<E> implements List<E> {
    private int max_size;

    public CustomList(int max_size) {
        this.max_size = max_size-1;
    }



    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        if ((c.size()>max_size)||(c.size()-this.size()>max_size)){
            try {
                throw new SizeException("Wrong size of collection");
            } catch (SizeException e) {
                e.printStackTrace();
            }
            return false;
        }else
        {
            return super.addAll(index, c);
        }
    }

    @Override
    public boolean add(E e) {
        System.out.println(this.size()+" "+max_size);
        if (this.size()>max_size){
            try {
                throw new SizeException("Wrong size of collection");
            } catch (SizeException ex) {
                ex.printStackTrace();
            }
            return false;
        }
        else {
            return super.add(e);
        }
    }

    @Override
    public void add(int index, E element) {

        if (this.size()>max_size){
            try {
                throw new SizeException("Wrong size of collection");
            } catch (SizeException ex) {
                ex.printStackTrace();
            }
            return;
        }
        super.add(index, element);
    }
}

