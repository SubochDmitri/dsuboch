import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        String[] s = Input.readText();
        HashMap<String, TreeSet<String>> map = Spitting.split(s);
        for (Map.Entry entry : map.entrySet()) {
            System.out.print(entry.getKey()+": ");
            for (String sa : (TreeSet<String>)entry.getValue()){
                System.out.print(sa+" ");
            }
            System.out.println();
        }
    }
}
