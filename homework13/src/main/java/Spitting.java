import java.util.HashMap;
import java.util.TreeSet;

public class Spitting {


    static HashMap<String, TreeSet<String>> split(String[] words){
        HashMap<String, TreeSet<String>> map = new HashMap<>();
        TreeSet<String> set = new TreeSet<>();
        char firstChar = words[0].charAt(0);
        for (int i=0;i<words.length;i++){
            if (firstChar==words[i].charAt(0)){
                set.add(words[i]);
            }
            else {
                map.put(String.valueOf(firstChar), (TreeSet<String>)set.clone());
                set.clear();
                set.add(words[i]);
                firstChar = words[i].charAt(0);
            }
        }
        return map;
    }
}
