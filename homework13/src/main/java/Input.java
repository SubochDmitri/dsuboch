import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Input {
    static String[] readText(){
        InputStreamReader cin = new InputStreamReader(System.in);
        BufferedReader bf = new BufferedReader(cin);
        String s="";
        try {
            s=bf.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] array = s.replaceAll("[,';:.()\\-!?\\s]+"," ").toLowerCase().trim().split(" ");
        Arrays.sort(array);

        return array;
    }
}
