public class DebitCard extends Card{
    public DebitCard(double balance, String name) {
        super(balance, name);
    }

    @Override
    public byte decreaseBalance(double size) {
        if (super.getBalance()<=size){
            System.out.println("Баланс меньше нуля");
            return 1;
        }
        else{
            super.decreaseBalance(size);
            return 0;
        }
    }

}
