public abstract class Card {
    private double balance;
    private String name;

    Card (double balance, String name)
    {
        this.balance = balance;
        this.name = name;
    }

    Card (String name)
    {
        this.name = name;
        balance = 0;
    }
    double getBalance()
    {
        return balance;
    }

    void increaseBalance(double size)
    {
        balance += size;
    }

    byte decreaseBalance(double size)
    {
        balance -= size;
        return 0;
    }

    void showBalanceInCurrency(double coof)
    {
        System.out.println(coof*balance);
    }
}
