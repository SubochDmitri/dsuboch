import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AtmTest {
    @Test
    void decreaseMoney() {
        Atm atm = new Atm();
        Card credit = new CreditCard(11,"Mali");
        Card debit = new DebitCard(0,"bolwoi");
        atm.getCard(credit);
        atm.increaseMoney(10);
        assertEquals(21,atm.getMoney());
        assertEquals(0,atm.decreaseMoney(10000));
        atm.getCard(debit);
        atm.increaseMoney(10);
        assertEquals(10,atm.getMoney());
        assertEquals(   1,atm.decreaseMoney(100));
    }

}