public class Atm {
    private Card card;

    public Atm(){

    }
    void getCard(Card card){
        this.card = card;
    }

    byte decreaseMoney(double size){
       return  card.decreaseBalance(size);
    }

    void increaseMoney(double size){
        card.increaseBalance(size);
    }

    double getMoney(){
        return card.getBalance();
    }
}
