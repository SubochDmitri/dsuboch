<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>Order</title>

</head>
<body>
<h1 align="center">Hello ${param.name} ${token}!</h1>
<form action="OrderServlet" method="POST">
    <div align="center">
        <p>Make your order</p>

            <select multiple size=6 name = "selected">
                <c:forEach var="good" items="${goods}">
                    <option value="${good.key}">${good.key} (${good.value}$)</option>
                </c:forEach>
            </select>

    <br>
        <input type="submit" name="submit" value="Submit" />
        <input type="hidden" name="name" value="${param.name }"/>

    </div>

</form>
</body>
</html>