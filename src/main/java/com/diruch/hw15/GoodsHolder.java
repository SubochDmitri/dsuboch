package com.diruch.hw15;

import java.util.HashMap;

public class GoodsHolder {
    private static final HashMap<String, Double> goods = new HashMap<String, Double>() {
        {
            put("Lada", 100.0);
            put("BMW", 300.0);
            put("Opel", 150.0);
            put("Citroen", 500.0);
            put("Mersedes", 8000.0);
            put("Bugatti", 25000.0);
        }
    };

    public static HashMap<String, Double> getGoods() {
        return goods;
    }
}
