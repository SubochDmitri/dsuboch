package com.diruch.hw15;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@WebServlet("/Goods")
public class Goods extends HttpServlet {


    public static final HashMap<String, Double> goods = new HashMap<String, Double>() {
        {
            put("Lada", 100.0);
            put("BMW", 300.0);
            put("Opel", 150.0);
            put("Citroen", 500.0);
            put("Mersedes", 8000.0);
            put("Bugatti", 25000.0);
        }
    };

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("goods", goods);
        getServletContext().getRequestDispatcher("/jsp/order.jsp").forward(req, resp);
    }
}
