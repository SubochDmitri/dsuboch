package com.diruch.hw15;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/OrderServlet")
public class OrderServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String[] values = req.getParameterValues("selected");
        PrintWriter writer = resp.getWriter();
        writer.write("<div align='center'>");
        writer.write("<h1>Dear " + req.getParameter("name") + ", your order:</h1><br><br>");

        double sum = 0;
        for (int i = 0; i < values.length; i++) {
            double price = GoodsHolder.getGoods().get(values[i]);
            sum += price;
            writer.write(i + 1 + ") " + values[i] + " " + price + "$<br>");
        }
        writer.write("Total: $" + sum);
        writer.write("</div>");

    }

}
