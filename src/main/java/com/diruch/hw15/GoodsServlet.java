package com.diruch.hw15;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

@WebServlet("/GoodsServlet")
public class GoodsServlet extends HttpServlet {



    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        boolean checkBox = req.getParameter( "agreewith" ) != null;
        if (checkBox){

            req.setAttribute("goods", GoodsHolder.getGoods());
            getServletContext().getRequestDispatcher("/jsp/order.jsp").forward(req, resp);

        } else {
            getServletContext().getRequestDispatcher("/jsp/errorpage.jsp").forward(req, resp);
        }


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
