public class SelectionSort implements Sorter {

    public int[] sort (int[] array){
        int min, tmp;
        for (int i = 0; i < array.length-1; i++){
            min = i;
            for (int scan = i+1; scan < array.length; scan++){
                if (array[scan] < array[min])
                    min = scan;
            }
            tmp = array[min];
            array[min] = array[i];
            array[i] = tmp;
        }
        return array;
    }
}
