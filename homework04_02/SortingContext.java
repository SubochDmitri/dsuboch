public class SortingContext {
    private Sorter sorter;

    public void setStrategy(Sorter sorter){
        this.sorter = sorter;
    }

    public int[] execute(int[] array) {
       return sorter.sort(array);
    }

}
