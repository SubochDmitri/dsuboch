public class Main {

    public static void main(String[] args) {
        int[] a = {2,45,123,56,21,34,12};
        int[] b = {2,45,123,56,21,232,5435};
        SortingContext sortingContext = new SortingContext();

        sortingContext.setStrategy(new BubbleSort());
        a = sortingContext.execute(a);

        sortingContext.setStrategy(new SelectionSort());
        b = sortingContext.execute(b);

        for (int i=0;i<b.length;i++){
            System.out.println(a[i]+" "+b[i]);
        }
    }
}
